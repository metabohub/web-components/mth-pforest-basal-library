// core test
import MthPfDisplayLink from '@/components/MthPfDisplayLink.vue'

// vuetifyjs imports
import vuetify from '@/plugins/vuetify';
const global = {
  plugins: [vuetify]
}

// overwrite navigator clipboard method
let clipboardContents = "";
Object.defineProperty(navigator, "clipboard", {
  value: {
    writeText: async (text: string) => {
      clipboardContents = text;
    },
    readText: () => clipboardContents,
  },
});

// overwrite navigator openUrl method
let open_url = ""
Object.defineProperty(window, "open", {
  value: (url) => { open_url = url }
});

describe('<MthPfDisplayLink />', () => {

  it('renders', () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(MthPfDisplayLink, { global })
    // test rendering
    cy.get('#input-0').get("input").should('have.value', "");
  })

  it('set option - linkLabel', () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(MthPfDisplayLink, { global, props: { linkLabel: "set by tests" } })
    // test rendering
    cy.get('#input-0').get('label').should('have.length', 2)
    cy.get('#input-0').get('label').each(($ele, index) => {
      if ($ele.text().includes('set by tests')) {
        cy.log("index", index) //logs the index
      }
    });//.should('have.text', "set by tests");

    cy.get('#input-0').get("input").should('have.value', "");
  })

  it('set option - linkValue', () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(MthPfDisplayLink, { global, props: { linkValue: "set by tests" } })
    // test rendering
    cy.get('#input-0').get("label").should('have.text', "");
    cy.get('#input-0').get("input").should('have.value', "set by tests");
    //cy.get('#input-0').get("url").should('have.text', "");
  })

  it('set option - linkUrl', () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(MthPfDisplayLink, { global, props: { linkUrl: "set by tests" } })
    // test rendering
    cy.get('#input-0').get("label").should('have.text', "");
    cy.get('#input-0').get("input").should('have.value', "set by tests");
    //cy.get('#input-0').get("url").should('have.text', "set by tests");
  })

  it('test action - copyToClipboard', () => {
    // reset clipboard
    clipboardContents = "";
    // mount component
    cy.mount(MthPfDisplayLink, { global, props: { linkValue: "data to copy in clipboard" } })
    // test rendering
    cy.get('#input-0').get("input").should('have.value', "data to copy in clipboard");
    cy.get(".v-input__append > .fas").should('have.class', 'fa-clipboard');
    // test clipboard before
    expect(clipboardContents).to.eq("");
    expect(window.navigator.clipboard.readText()).to.eq("");
    // copy action
    cy.get('#input-0').click();
    // test rendering
    cy.get(".v-input__append > .fas").should('have.class', 'fa-circle-check fa-bounce');
    // test clipboard after
    cy.wait(50).then(() => {
      expect(clipboardContents).to.eq("data to copy in clipboard");
      expect(window.navigator.clipboard.readText()).to.eq("data to copy in clipboard");
    })
    // test after more than 1800 ms
    cy.wait(3000).then(() => {
      cy.get(".v-input__append > .fas").should('have.class', 'fa-clipboard');
    })
  })

  it('mode edition', () => {
    // reset clipboard
    clipboardContents = "";
    // see: https://on.cypress.io/mounting-vue 
    cy//
      .mount(MthPfDisplayLink, { global, props: { linkValue: "set by tests", isReadonly: false, linkPlaceholder: "lorem ipsum" } })//
      .as('wrapper')
    // test rendering
    cy.get('#input-0').get("label").should('have.text', "");
    cy.get('#input-0').get("input").should('have.value', "set by tests");
    // set to empty → should display placeholder
    cy.get('#input-0').get("input").clear();
    cy.get('#input-0').get("input").should('have.value', "");
    cy.get('#input-0').invoke('attr', 'placeholder').should('contain', 'lorem ipsum')
    // set new value
    cy.get('#input-0').get("input").type('new value');
    cy.get('#input-0').get("input").should('have.value', "new value");
    // test emiter
    cy.get('@wrapper').should(({ wrapper }) => {
      expect(wrapper.emitted('update-link-value')).to.have.length
      expect(wrapper.emitted('update-link-value')[9][0]).to.equal('new value')
    })
    // test copyToClipboard disabled if click on text
    expect(window.navigator.clipboard.readText()).to.eq("");
    cy.get('#input-0').click();
    cy.wait(50).then(() => {
      expect(clipboardContents).to.eq("");
      expect(window.navigator.clipboard.readText()).to.eq("");
    })
    //   // remove mode edition
    //   cy.get('@wrapper').invoke('setProps', { isReadonly: true })    
    // //  test copyToClipboard re-enabled if click on text
    //   expect(window.navigator.clipboard.readText()).to.eq("");
    //   cy.get('#input-0').click();
    //   cy.wait(50).then(() => {
    //     expect(clipboardContents).to.eq("new value");
    //     expect(window.navigator.clipboard.readText()).to.eq("new value");
    //   })

  })

  it('test action openLink', () => {
    // reset open_url
    open_url = "";
    // mount component
    cy.mount(MthPfDisplayLink,
      {
        global, props: {
          linkLabel: "PeakForest ID",
          linkValue: "PFc00000186",
          linkUrl: "https://metabohub.peakforest.org/webapp/PFc00000186",
        }
      }
    )
    // test rendering
    cy.get('#input-0').get("input").should('have.value', "PFc00000186");
    cy.get(".fa-solid").should('have.class', 'fa-link');
    // test open_url before
    expect(open_url).to.eq("");
    // copy action
    cy.get(".fa-solid").click();
    // test open_url after
    cy.wait(50).then(() => {
      expect(open_url).to.eq("https://metabohub.peakforest.org/webapp/PFc00000186");
    })
  });

})
