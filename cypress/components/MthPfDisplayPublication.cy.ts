// core test
import MthPfDisplayPublication from '@/components/MthPfDisplayPublication.vue'

// vuetifyjs imports
import vuetify from '@/plugins/vuetify';
const global = {
  plugins: [vuetify]
}

// overwrite navigator clipboard method
let clipboardContents = "";
Object.defineProperty(navigator, "clipboard", {
  value: {
    writeText: async (text: string) => {
      clipboardContents = text;
    },
    readText: () => clipboardContents,
  },
});

describe('<MthPfDisplayPublication />', () => {

  // default / readonly
  it('renders', () => {
    // mock
    cy.intercept(//
      'GET', //
      'https://www.ebi.ac.uk/europepmc/webservices/rest/search?query=x&format=json&domain=literature', //
      { fixture: "get_publication_succes.json" });
    // mount
    cy.mount(MthPfDisplayPublication, {
      global, props: { //
        publicationIdentifier: "x"
      }
    })
    const textAPA = 'Pujos-Guillot, E., Pétéra, M., Jacquemin, J., Centeno, D., Lyan, B., Montoliu, I., Madej, D., Pietruszka, B., Fabbri, C., Santoro, A., Brzozowska, A., Franceschi, C. & Comte, B. (2018). Identification of Pre-frailty Sub-Phenotypes in Elderly Using Metabolomics. Front Physiol, 9, 1903. 10.3389/fphys.2018.01903.';
    // test rendering
    cy.get('.v-card-title > a').should('have.text', "Pujos-Guillot et al. 2018");
    cy.get('.v-card-title > a').should('have.html', "Pujos-Guillot <em>et al.</em> 2018");
    cy.get('.v-card-subtitle > span').should('have.text', "10.3389/fphys.2018.01903");
    cy.get('.v-card-text > div').should('have.text', textAPA);
    cy.get('.v-card-actions > a').should('have.text', "30733683");
    cy.get('.v-btn').should('not.exist');
    // test action btn 1
    clipboardContents = '';
    expect(clipboardContents).to.eq("");
    cy.get('.v-card-title > .fa').click();
    cy.wait(50).then(() => {
      expect(clipboardContents).to.eq("Pujos-Guillot et al. 2018");
      expect(window.navigator.clipboard.readText()).to.eq("Pujos-Guillot et al. 2018");
    });
    // test action btn 2  
    clipboardContents = '';
    expect(clipboardContents).to.eq("");
    cy.get('.v-card-subtitle > .fa').click();
    cy.wait(50).then(() => {
      expect(clipboardContents).to.eq("10.3389/fphys.2018.01903");
      expect(window.navigator.clipboard.readText()).to.eq("10.3389/fphys.2018.01903");
    });
    // test action btn 3 
    clipboardContents = '';
    expect(clipboardContents).to.eq("");
    cy.get('.v-card-text > .fa').click();
    cy.wait(50).then(() => {
      expect(clipboardContents).to.contain("Pujos-Guillot");
      expect(window.navigator.clipboard.readText()).to.contain("Pujos-Guillot");
    });
    // test action btn 4
    clipboardContents = '';
    expect(clipboardContents).to.eq("");
    cy.get('.v-card-actions > .fa').click();
    cy.wait(50).then(() => {
      expect(clipboardContents).to.eq("30733683");
      expect(window.navigator.clipboard.readText()).to.eq("30733683");
    });
    // test link 1 
    cy.get('.v-card-title > a')
      .should('have.attr', 'href', 'https://doi.org/10.3389/fphys.2018.01903')
      .should('have.attr', 'target', '_blank')
      .should('have.attr', 'rel', 'noopener');
    // test link 2
    cy.get('.v-card-actions > a')
      .should('have.attr', 'href', 'https://pubmed.ncbi.nlm.nih.gov/30733683')
      .should('have.attr', 'target', '_blank')
      .should('have.attr', 'rel', 'noopener');
  });

  // with trash button
  it('renders', () => {
    // mock
    cy.intercept(//
      'GET', //
      'https://www.ebi.ac.uk/europepmc/webservices/rest/search?query=x&format=json&domain=literature', //
      { fixture: "get_publication_succes.json" });
    // mount
    cy.mount(MthPfDisplayPublication, {
      global, props: { //
        publicationIdentifier: "x",
        isReadonly: false,
      }
    }).as('wrapper')
    // test rendering
    cy.get('.v-btn').should('exist');
    // test action 
    cy.get('.v-btn').click();
    cy.get('@wrapper').should(({ wrapper }) => {
      expect(wrapper.emitted('remove-publication')).to.have.length
      expect(wrapper.emitted('remove-publication')[0][0]).to.equal('x');
    })
  });

})
