import MthPfSwitchTheme from '@/components/MthPfSwitchTheme.vue'

import vuetify from '@/plugins/vuetify';
import { useTheme } from 'vuetify/lib/framework.mjs';

const global = {
  plugins: [vuetify]
}

describe('<MthPfSwitchTheme />', () => {

  beforeEach(() => {
    window.localStorage.clear();
  })

  afterEach(() => {
    window.localStorage.clear();
  })

  it('renders', () => {
    cy.mount(MthPfSwitchTheme, { global });
    cy.get('span').should('not.be.empty');
    cy.get('i').should('contains.class', 'fas fa-sun');
    cy.get('.v-switch__track').should('contains.class', 'bg-primary');
  });

  it('set option - displayThemeText', () => {
    cy.mount(MthPfSwitchTheme, { global, props: { displayThemeText: true } });
    cy.get('span').should('have.text', 'Theme: light');
    cy.get('i').should('contains.class', 'fas fa-sun');
  });

  it('set option - displayThemeIcon', () => {
    cy.mount(MthPfSwitchTheme, { global, props: { displayThemeIcon: false } });
    cy.get('span').should('not.be.empty');
    cy.get('i').should('not.exist');
  });

  it('test action', () => {
    cy.mount(MthPfSwitchTheme, { global }).as('wrapper');
    // test before
    cy.get('i').should('contains.class', 'fas fa-sun');
    cy.get('html').should('have.css', 'background-color', 'rgba(0, 0, 0, 0)')
    cy.getAllLocalStorage().then((result) => {
      expect(result).to.deep.that.is.empty;
    })
    // action
    cy.get('input').click();
    // test after
    cy.get('i').should('contains.class', 'fas fa-moon');

    cy.getAllLocalStorage().then((result) => {
      expect(result).to.deep.include({ "http://localhost:3001": { "mth-pf-switch-theme": 'dark' } })
    })

    cy.get('@wrapper').should(({ wrapper }) => { 
      expect(wrapper.emitted('update-theme')).to.have.length
      expect(wrapper.emitted('update-theme')[0][0]).to.equal('dark')
    })

  });

  it('test action, init with local storage ', () => {
    // WARNING - init like that the component is "read only" like
    // init
    window.localStorage.setItem('mth-pf-switch-theme', 'dark')
    cy.mount(MthPfSwitchTheme, { global });
    // test before
    cy.get('i').should('contains.class', 'fas fa-moon');
    cy.getAllLocalStorage().then((result) => {
      expect(result).to.deep.include({ "http://localhost:3001": { "mth-pf-switch-theme": 'dark' } })
    })
    // action
    cy.get('input').click();
    // test after
    cy.get('i').should('contains.class', 'fas fa-sun');
    cy.getAllLocalStorage().then((result) => {
      expect(result).to.deep.include({ "http://localhost:3001": { "mth-pf-switch-theme": 'light' } })
    })
  });

  it('test action, set from parent ', () => {
    // init  
    cy.mount(MthPfSwitchTheme, {
      global, setup() {
        const themObjVal = useTheme() 
        themObjVal.global.name.value = 'dark';
        return {
          // get current theme
          themeObj: themObjVal,
          // model and icon
          theme: "dark",
          themeIcon: "",
          // key for local storage
          themeLSKey: "mth-pf-switch-theme",
          // display options
          isDisplayThemeText: true,
          isDisplayThemeIcon: true,
        }
      }
    });
    // test  
    cy.get('i').should('contains.class', 'fas fa-moon');
    cy.get('span').should('have.text', 'Theme: dark');
    cy.getAllLocalStorage().then((result) => {
      expect(result).to.deep.that.is.empty;
    }) 
  });

})
