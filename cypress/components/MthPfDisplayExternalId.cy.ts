// core test
import MthPfDisplayExternalId from '@/components/MthPfDisplayExternalId.vue'

// vuetifyjs imports
import vuetify from '@/plugins/vuetify';
const global = {
  plugins: [vuetify]
}

// overwrite navigator clipboard method
let clipboardContents = "";
Object.defineProperty(navigator, "clipboard", {
  value: {
    writeText: async (text: string) => {
      clipboardContents = text;
    },
    readText: () => clipboardContents,
  },
});

// overwrite navigator openUrl method
let open_url = ""
Object.defineProperty(window, "open", {
  value: (url) => { open_url = url }
});

describe('<MthPfDisplayExternalId />', () => {

  it('renders', () => {
    cy.mount(MthPfDisplayExternalId, { global })
    // test rendering
    cy.get('#input-0').get("input").should('have.value', "");
  })

  it('set options', () => {
    // mount
    cy.mount(MthPfDisplayExternalId,
      {
        global, props: {
          identifierLabel: "PeakForest ID",
          identifierValue: "186",
          identifierUrlPrefix: "https://metabohub.peakforest.org/webapp/PFc",
          identifierSuffixLenght: "8",
          identifierPrefix: "PFc",
        }
      }
    )
    // test rendering
    cy.get('#input-0').get('label').should('have.length', 2)
    cy.get('#input-0').get('label').each(($ele, index) => {
      if ($ele.text().includes('PeakForest ID')) {
        cy.log("index", index) //logs the index
      }
    });
    cy.get('#input-0').get("input").should('have.value', "PFc00000186");
  })

  it('test action copyToClipboard', () => {
    // reset clipboard
    clipboardContents = "";
    // mount component
    cy.mount(MthPfDisplayExternalId,
      {
        global, props: {
          identifierLabel: "PeakForest ID",
          identifierValue: "186",
          identifierUrlPrefix: "https://metabohub.peakforest.org/webapp/PFc",
          identifierSuffixLenght: "8",
          identifierPrefix: "PFc",
        }
      }
    )
    // test rendering
    cy.get('#input-0').get("input").should('have.value', "PFc00000186");
    cy.get(".v-input__append > .fas").should('have.class', 'fa-clipboard');
    // test clipboard before
    expect(clipboardContents).to.eq("");
    expect(window.navigator.clipboard.readText()).to.eq("");
    // copy action
    cy.get('#input-0').click();
    // test rendering
    cy.get(".v-input__append > .fas").should('have.class', 'fa-circle-check fa-bounce');
    // test clipboard after
    cy.wait(50).then(() => {
      expect(clipboardContents).to.eq("PFc00000186");
      expect(window.navigator.clipboard.readText()).to.eq("PFc00000186");
    })
    // test after more than 1800 ms
    cy.wait(3000).then(() => {
      cy.get(".v-input__append > .fas").should('have.class', 'fa-clipboard');
    })
  })

  it('test action openLink', () => {
    // reset open_url
    open_url = "";
    // mount component
    cy.mount(MthPfDisplayExternalId,
      {
        global, props: {
          identifierLabel: "PeakForest ID",
          identifierValue: "186",
          identifierUrlPrefix: "https://metabohub.peakforest.org/webapp/PFc",
          identifierSuffixLenght: "8",
          identifierPrefix: "PFc",
        }
      }
    )
    // test rendering
    cy.get('#input-0').get("input").should('have.value', "PFc00000186");
    cy.get(".fa-solid").should('have.class', 'fa-link');
    // test open_url before
    expect(open_url).to.eq("");
    // copy action
    cy.get(".fa-solid").click();
    // test open_url after
    cy.wait(50).then(() => {
      expect(open_url).to.eq("https://metabohub.peakforest.org/webapp/PFc186");
    })
  });

})
