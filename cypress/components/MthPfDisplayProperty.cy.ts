// core test
import MthPfDisplayProperty from '@/components/MthPfDisplayProperty.vue'

// vuetifyjs imports
import vuetify from '@/plugins/vuetify';
const global = {
  plugins: [vuetify]
}

// overwrite navigator clipboard method
let clipboardContents = "";
Object.defineProperty(navigator, "clipboard", {
  value: {
    writeText: async (text: string) => {
      clipboardContents = text;
    },
    readText: () => clipboardContents,
  },
});

describe('<MthPfDisplayProperty />', () => {

  it('renders', () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(MthPfDisplayProperty, { global })
    // test rendering
    cy.get('#input-0').get("input").should('have.value', "");
  })

  it('set option - propertyLabel', () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(MthPfDisplayProperty, { global, props: { propertyLabel: "set by tests" } })
    // test rendering
    cy.get('#input-0').get('label').should('have.length', 2)
    cy.get('#input-0').get('label').each(($ele, index) => {
      if ($ele.text().includes('set by tests')) {
        cy.log("index", index) //logs the index
      }
    });//.should('have.text', "set by tests");

    cy.get('#input-0').get("input").should('have.value', "");
  })

  it('set option - propertyValue', () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(MthPfDisplayProperty, { global, props: { propertyValue: "set by tests" } })
    // test rendering
    cy.get('#input-0').get("label").should('have.text', "");
    cy.get('#input-0').get("input").should('have.value', "set by tests");
  })

  it('test action', () => {
    // reset clipboard
    clipboardContents = "";
    // mount component
    cy.mount(MthPfDisplayProperty, { global, props: { propertyValue: "data to copy in clipboard" } })
    // test rendering
    cy.get('#input-0').get("input").should('have.value', "data to copy in clipboard");
    cy.get(".v-input__append > .fas").should('have.class', 'fa-clipboard');
    // test clipboard before
    expect(clipboardContents).to.eq("");
    expect(window.navigator.clipboard.readText()).to.eq("");
    // copy action
    cy.get('#input-0').click();
    // test rendering
    cy.get(".v-input__append > .fas").should('have.class', 'fa-circle-check fa-bounce');
    // test clipboard after
    cy.wait(50).then(() => {
      expect(clipboardContents).to.eq("data to copy in clipboard");
      expect(window.navigator.clipboard.readText()).to.eq("data to copy in clipboard");
    })
    // test after more than 1800 ms
    cy.wait(3000).then(() => {
      cy.get(".v-input__append > .fas").should('have.class', 'fa-clipboard');
    });
  })

  it('mode edition', () => {
    // reset clipboard
    clipboardContents = "";
    // see: https://on.cypress.io/mounting-vue 
    cy//
      .mount(MthPfDisplayProperty, { global, props: { propertyValue: "set by tests", isReadonly: false, propertyPlaceholder: "lorem ipsum" } })//
      .as('wrapper')
    // test rendering
    cy.get('#input-0').get("label").should('have.text', "");
    cy.get('#input-0').get("input").should('have.value', "set by tests");
    // set to empty → should display placeholder
    cy.get('#input-0').get("input").clear();
    cy.get('#input-0').get("input").should('have.value', "");
    cy.get('#input-0').invoke('attr', 'placeholder').should('contain', 'lorem ipsum')
    // set new value
    cy.get('#input-0').get("input").type('new value');
    cy.get('#input-0').get("input").should('have.value', "new value");
    // test emiter
    cy.get('@wrapper').should(({ wrapper }) => { 
      expect(wrapper.emitted('update-property-value')).to.have.length
      expect(wrapper.emitted('update-property-value')[9][0]).to.equal('new value')
    })
    // test copyToClipboard disabled if click on text
    expect(window.navigator.clipboard.readText()).to.eq("");
    cy.get('#input-0').click();
    cy.wait(50).then(() => {
      expect(clipboardContents).to.eq("");
      expect(window.navigator.clipboard.readText()).to.eq("");
    })
    //   // remove mode edition
    //   cy.get('@wrapper').invoke('setProps', { isReadonly: true })    
    // //  test copyToClipboard re-enabled if click on text
    //   expect(window.navigator.clipboard.readText()).to.eq("");
    //   cy.get('#input-0').click();
    //   cy.wait(50).then(() => {
    //     expect(clipboardContents).to.eq("new value");
    //     expect(window.navigator.clipboard.readText()).to.eq("new value");
    //   })

  })

})
