import MthPfDisplayScoredValue from '@/components/MthPfDisplayScoredValue.vue';

import vuetify from '@/plugins/vuetify';

const global = {
    plugins: [vuetify]
}

describe('MthPfDisplayScoredValue />', () => {

    it('renders with default props', () => {
        cy.mount(MthPfDisplayScoredValue, {
            global, props: {
                scoreValue: 0,
                nbStars: 3,
            }
        });
        // Ensure the component and v-rating exist
        cy.get('.v-rating').should('exist');
        // Ensure v-chip is not present with default props
        cy.get('.v-chip').should('not.exist');
    });

    it('renders with custum props: show-ship, edit mode', () => {
        cy.mount(MthPfDisplayScoredValue, {
            global, props: {
                scoreValue: 0,
                nbStars: 3,
                showChip: true,
                isReadonly: false,
            }
        });
        // Ensure the component and v-rating exist
        cy.get('.v-rating').should('exist');
        // Ensure v-chip is present with default props
        cy.get('.v-chip').should('exist');
        cy.get('.v-chip').should('have.text', 0);
        // try update via click
        cy.get(':nth-child(3) > .v-rating__item > .v-rating__item--full').trigger('click');
        cy.get('.v-chip').should('have.text', 1);
        cy.get(':nth-child(4) > .v-rating__item > .v-rating__item--full').trigger('click');
        cy.get('.v-chip').should('have.text', 2);
        cy.get(':nth-child(5) > .v-rating__item > .v-rating__item--full').trigger('click');
        cy.get('.v-chip').should('have.text', 3);
    });

    it('renders with custum props: show-ship, readonly mode', () => {
        cy.mount(MthPfDisplayScoredValue, {
            global, props: {
                scoreValue: 0,
                nbStars: 3,
                showChip: true,
                isReadonly: true,
            }
        });
        // Ensure the component and v-rating exist
        cy.get('.v-rating').should('exist');
        // Ensure v-chip is present with default props
        cy.get('.v-chip').should('exist');
        cy.get('.v-chip').should('have.text', 0);
        // try update via click
        cy.get(':nth-child(3) > .v-rating__item > .v-rating__item--full').trigger('click');
        cy.get('.v-chip').should('have.text', 0);
        cy.get(':nth-child(4) > .v-rating__item > .v-rating__item--full').trigger('click');
        cy.get('.v-chip').should('have.text', 0);
        cy.get(':nth-child(5) > .v-rating__item > .v-rating__item--full').trigger('click');
        cy.get('.v-chip').should('have.text', 0);
    });

    it('renders with custum props, but change them', () => {
        cy.mount(MthPfDisplayScoredValue, {
            global, //
            props: {
                scoreValue: 1,
                nbStars: 5,
                showChip: false,
                isReadonly: false,
            }
        });
        // try update via click
        cy.get(':nth-child(3) > .v-rating__item > .v-rating__item--full').trigger('click').then(() => {
            // test emit
            cy.wrap(Cypress.vueWrapper.emitted()).should('have.property', 'update-score-value');
            expect(Cypress.vueWrapper.emitted()['update-score-value'][0]).to.deep.equal([1]);
            expect(Cypress.vueWrapper.emitted()['update-score-value'][1]).to.deep.equal([0.2]);
        });
    });
});