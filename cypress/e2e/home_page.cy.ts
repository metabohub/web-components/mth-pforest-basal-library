


describe('The Home Page', () => {
  it('successfully loads | full URL', () => {
    // connect
    cy.visit('http://localhost:3000');
    // test header / left menu / footer
    cy.contains("MetaboHUB | PeakForest Basal - Library");
    cy.contains("How to Install");
    cy.contains("Powered by MetaboHUB | 2023");
    // test URL
    cy.url().should('include', '#/how-to-install');
    // cy.getAllLocalStorage().then((result) => {
    //   console.log("xxx",result);
    //   expect(result).to.deep.equal({
    //     'http://localhost:3000': {
    //       'mth-pf-switch-theme': 'light',
    //     },
    //   })
    // })
  })
  it('successfully loads | just relative URL', () => {
    // connect
    cy.visit('/');
    // test header / left menu / footer
    cy.contains("MetaboHUB | PeakForest Basal - Library");
    cy.contains("How to Install");
    cy.contains("Powered by MetaboHUB | 2023");
    // test URL
    cy.url().should('include', '#/how-to-install')
  })
})