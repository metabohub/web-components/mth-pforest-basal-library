# Change Log | `MetaboHUB / PeakForest - Basal Library`

## About

All notable changes to `MetaboHUB / PeakForest - Basal Library` project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - 2023-05-25

Initial release of the library.

### Added

- [pforest#9]
  MINOR - add basic web-components: `<mth-pf-display-property />`
- [pforest#19]
  MINOR - add basic web-components: `<mth-pf-switch-theme />`
