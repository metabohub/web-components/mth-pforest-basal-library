
export { copyPropertyValueToClipboard, copyTextToClipboard }

/**
 * Copy the textContent or innerText of a HTML element to the clipboard
 * 
 * @param element targeted HTML element to copy its textContent or innerText
 */
function copyPropertyValueToClipboard(element: HTMLInputElement) {
    // convert HTML formatted text to plain text
    const tempDivElement = document.createElement("div");
    tempDivElement.innerHTML = element.value;
    const text = tempDivElement.textContent || tempDivElement.innerText;
    // put text into clipboard
    copyTextToClipboard(text);
}

function copyTextToClipboard(text: string) {
    // put text into clipboard
    navigator.clipboard.writeText(text).then(
        function () { /* clipboard successfully set */ },
        function () { /* clipboard write failed     */ }
    );
}
