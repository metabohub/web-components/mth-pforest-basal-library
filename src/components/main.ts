import type { App } from 'vue';
// in-house components
import {
    // components
    MthPfDisplayExternalId, //
    MthPfDisplayPublication, //
    MthPfDisplayProperty, //
    MthPfDisplayLink, //
    MthPfDisplayScoredValue, //
    MthPfSwitchTheme, //
    // enum
    PropertyTypeEnum,//
} from "@/components";
// in-house ts code
import {
    copyPropertyValueToClipboard, //
    copyTextToClipboard,//
    type Status, //
} from "@/components/utils";
import { //
    Publication, //
    LiteratureServiceResponse, //
    getPublicationFromIdentifier, //
} from "@/services/LiteratureService";
// export lib
const MetabohubPeakForestBasalLibrary = {
    install: (app: App) => {
        // PeakForest basal components
        app.component('MthPfDisplayExternalId', MthPfDisplayExternalId);
        app.component('MthPfDisplayPublication', MthPfDisplayPublication);
        app.component('MthPfDisplayProperty', MthPfDisplayProperty);
        app.component('MthPfDisplayLink', MthPfDisplayLink);
        app.component('MthPfDisplayScoredValue', MthPfDisplayScoredValue);
        app.component('MthPfSwitchTheme', MthPfSwitchTheme);
    },
};

export {
    // all components
    MetabohubPeakForestBasalLibrary, //
    // TS code
    copyPropertyValueToClipboard, //
    copyTextToClipboard, //
    // types
    type Status, //
    // Enums
    PropertyTypeEnum, //
    // literature mgmt
    type Publication, //
    type LiteratureServiceResponse, // 
    getPublicationFromIdentifier, //
};
