import { expect, describe, test, afterAll } from "vitest";

// core test 
import { //
    getPublicationFromIdentifier,//
    LiteratureServiceResponse,//
} from '@/services/LiteratureService'

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const defaultResponse = {
    "version": "6.9",
    "hitCount": 0,
    "request": {
        "queryString": "XXX",
        "resultType": "lite",
        "cursorMark": "*",
        "pageSize": 25,
        "sort": "",
        "synonym": false
    },
    "resultList": {
        "result": [] as any[]
    }
};

const serverOK1 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock1";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            pubYear: '',
            title: '',
            journalTitle: '',
        });
        return res(
            ctx.json(okJson),
        )
    }),
);

const serverOK2 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock2";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            pubYear: '2023',
            title: 'Sample Title',
            journalTitle: 'Sample Journal',
        });
        return res(
            ctx.json(okJson),
        )
    }),
);

const serverOK3 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock3";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            pubYear: '',
            title: 'Sample Title',
            journalTitle: 'Sample Journal',
        });
        return res(
            ctx.json(okJson),
        )
    }),
);

const serverOK4 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock4";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            pubYear: '2023',
            title: '',
            journalTitle: 'Sample Journal',
        });
        return res(
            ctx.json(okJson),
        )
    }),
);

const serverOK5 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock4";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            pubYear: '2023',
            title: 'Sample Title',
            journalTitle: '',
        });
        return res(
            ctx.json(okJson),
        )
    }),
);

afterAll(() => {
    // Clean up after all tests are done, preventing this
    // interception layer from affecting irrelevant tests.
    serverOK1.close();
    serverOK2.close();
    serverOK3.close();
    serverOK4.close();
    serverOK5.close();
})

describe('getApaMainData function', () => {

    test('should return an empty string if all fields are missing', async () => {
        serverOK1.listen()
        await getPublicationFromIdentifier("mock1").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication.apa_html).toBe('');
        });
        serverOK1.close();
    });

    test('should return the APA formatted data with the publication year', async () => {
        serverOK2.listen()
        await getPublicationFromIdentifier("mock2").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication.apa_html).toBe('(2023). Sample Title <em>Sample Journal</em>.');
        });
        serverOK2.close();
    });

    test('should return the APA formatted data without the publication year if it is missing', async () => {
        serverOK3.listen()
        await getPublicationFromIdentifier("mock3").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication.apa_html).toBe('Sample Title <em>Sample Journal</em>.');
        });
        serverOK3.close();
    });

    test('should return the APA formatted data without the title if it is missing', async () => {
        serverOK4.listen()
        await getPublicationFromIdentifier("mock4").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication.apa_html).toBe('(2023). <em>Sample Journal</em>.');
        });
        serverOK4.close();
    });

    test('should return the APA formatted data without the journal title if it is missing', async () => {
        serverOK5.listen()
        await getPublicationFromIdentifier("mock5").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication.apa_html).toBe('(2023). Sample Title .');
        });
        serverOK5.close();
    });
});
