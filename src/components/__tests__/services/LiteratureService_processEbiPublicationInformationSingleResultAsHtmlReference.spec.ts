import { expect, describe, test, afterAll } from "vitest";

// core test 
import { //
    getPublicationFromIdentifier,//
    LiteratureServiceResponse,//
} from '@/services/LiteratureService'

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const defaultResponse = {
    "version": "6.9",
    "hitCount": 0,
    "request": {
        "queryString": "XXX",
        "resultType": "lite",
        "cursorMark": "*",
        "pageSize": 25,
        "sort": "",
        "synonym": false
    },
    "resultList": {
        "result": [] as any[]
    }
};

const serverOK1 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock1";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            authorString: 'John Doe, Jane Smith',
            pubYear: '2023',
        });
        return res(
            ctx.json(okJson),
        )
    }),
);



afterAll(() => {
    // Clean up after all tests are done, preventing this
    // interception layer from affecting irrelevant tests.
    serverOK1.close();
})

describe('processEbiPublicationInformationSingleResultAsHtmlReference function', () => {
    test('should return a correctly formatted reference with author and publication year', async () => {
        serverOK1.listen()
        await getPublicationFromIdentifier("mock1").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication.apa_html).toBe('John, Doe. &amp; Jane, Smith (2023). .');
        });
        serverOK1.close();
    });
});
