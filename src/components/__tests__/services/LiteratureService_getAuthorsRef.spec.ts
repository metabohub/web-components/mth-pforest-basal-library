import { expect, describe, test, afterAll } from "vitest";

// core test 
import { //
    getPublicationFromIdentifier,//
    LiteratureServiceResponse,//
} from '@/services/LiteratureService'

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const defaultResponse = {
    "version": "6.9",
    "hitCount": 0,
    "request": {
        "queryString": "XXX",
        "resultType": "lite",
        "cursorMark": "*",
        "pageSize": 25,
        "sort": "",
        "synonym": false
    },
    "resultList": {
        "result": [] as any[]
    }
};

const serverOK1 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock1";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            authorString: '',
        });
        return res(
            ctx.json(okJson),
        )
    }),
);

const serverOK2 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock2";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            authorString: 'John Doe',
        });
        return res(
            ctx.json(okJson),
        )
    }),
);

const serverOK3 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock3";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            authorString: 'John Doe, Jane Smith'
        });
        return res(
            ctx.json(okJson),
        )
    }),
);

const serverOK4 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock4";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            authorString: 'John Doe, Jane Smith, Alice Johnson',
        });
        return res(
            ctx.json(okJson),
        )
    }),
);


afterAll(() => {
    // Clean up after all tests are done, preventing this
    // interception layer from affecting irrelevant tests.
    serverOK1.close();
    serverOK2.close();
    serverOK3.close();
    serverOK4.close();
})

describe('getAuthorsRef function', () => {
    test('should return an empty string if authorString is missing', async () => {
        serverOK1.listen()
        await getPublicationFromIdentifier("mock1").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication.apa_html).toBe('');
        });
        serverOK1.close();
    });

    test('should return the first author if there is only one author', async () => {
        serverOK2.listen()
        await getPublicationFromIdentifier("mock2").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication.apa_html).toBe('John, Doe .');
        });
        serverOK2.close();
    });

    test('should return the first and second author separated by "&" if there are two authors', async () => {
        serverOK3.listen()
        await getPublicationFromIdentifier("mock3").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication.apa_html).toBe('John, Doe. &amp; Jane, Smith .');
        });
        serverOK3.close();
    });

    test('should return the first author and "et al." if there are more than two authors', async () => {
        serverOK4.listen()
        await getPublicationFromIdentifier("mock4").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication.apa_html).toBe('John, Doe., Jane, Smith. &amp; Alice, Johnson .');
        });
        serverOK4.close();
    });
});
