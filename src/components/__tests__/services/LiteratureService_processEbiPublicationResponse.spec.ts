import { expect, describe, test, afterAll } from "vitest";

// core test 
import { //
    getPublicationFromIdentifier,//
    LiteratureServiceResponse,//
    LiteratureServiceResponseStatusEnum,//
} from '@/services/LiteratureService'

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const defaultResponse = {
    "version": "6.9",
    "hitCount": 0,
    "request": {
        "queryString": "XXX",
        "resultType": "lite",
        "cursorMark": "*",
        "pageSize": 25,
        "sort": "",
        "synonym": false
    },
    "resultList": {
        "result": [] as any[]
    }
};

const serverOK1 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 0;
        okJson.request.queryString = "mock1";
        okJson.resultList.result = [];
        return res(
            ctx.json(okJson),
        )
    }),
);

const serverOK2 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock2";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            doi: 'sample-doi',
            pmid: 'sample-pmid',
            pmcid: 'sample-pmcid',
        });
        return res(
            ctx.json(okJson),
        )
    }),
);

const serverOK3 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 3;
        okJson.request.queryString = "mock3";
        okJson.resultList.result = [
            {
                doi: 'doi-1',
                pmid: 'pmid-1',
                pmcid: 'pmcid-1',
            },
            {
                doi: 'doi-2',
                pmid: 'pmid-2',
                pmcid: 'pmcid-2',
            },
            {
                doi: 'mock3',
                pmid: 'pmid-3',
                pmcid: 'pmcid-3',
            },
        ]
        return res(
            ctx.json(okJson),
        )
    }),
);


const serverKO1 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const koJson = {
            "version": "6.9",
            "hitCount": 1,
            "request": {
            },
            "resultList": {}
        };
        return res(
            ctx.json(koJson),
        )
    }),
);



afterAll(() => {
    // Clean up after all tests are done, preventing this
    // interception layer from affecting irrelevant tests.
    serverOK1.close();
    serverOK2.close();
    serverOK3.close();
    serverKO1.close();
})

describe('processEbiPublicationResponse function', () => {
    test('should return a correctly processed LiteratureServiceResponse with status NoResult', async () => {
        serverOK1.listen()
        await getPublicationFromIdentifier("mock1").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.status).toBe(LiteratureServiceResponseStatusEnum.NoResult);
            expect(lsr.publication).toBe(undefined);
            expect(lsr.error).toBe(undefined);
        });
        serverOK1.close();
    });

    test('should return a correctly processed LiteratureServiceResponse with status Success (single result)', async () => {
        serverOK2.listen()
        await getPublicationFromIdentifier("mock2").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.status).toBe(LiteratureServiceResponseStatusEnum.Success);
            expect(lsr.publication.doi).toBe('sample-doi');
            expect(lsr.publication.pubmed_id).toBe('sample-pmid');
            expect(lsr.publication.pubmed_cid).toBe('sample-pmcid');
            expect(lsr.error).toBe(undefined);
        });
        serverOK2.close();
    });

    test('should return a correctly processed LiteratureServiceResponse with status Success (multiple results)', async () => {
        serverOK3.listen()
        await getPublicationFromIdentifier("mock3").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.status).toBe(LiteratureServiceResponseStatusEnum.Success);
            expect(lsr.publication.doi).toBe('mock3');
            expect(lsr.publication.pubmed_id).toBe('pmid-3');
            expect(lsr.publication.pubmed_cid).toBe('pmcid-3');
            expect(lsr.error).toBe(undefined);
        });
        serverOK3.close();
    });

    test('error: non-consistance response', async () => {
        serverKO1.listen()
        await getPublicationFromIdentifier("mock_ko1").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.status).toBe(LiteratureServiceResponseStatusEnum.UnknownError);
            expect(lsr.publication).toBe(undefined);
            expect(lsr.error).toBe(undefined);
        });
        serverKO1.close();
    });
});
