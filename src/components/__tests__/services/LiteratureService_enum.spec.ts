import { expect, describe, it } from "vitest";

import {
    Publication,
    LiteratureServiceResponse,
    LiteratureServiceResponseStatusEnum,
} from '@/services/LiteratureService'; // Replace with the actual path to your code file

describe('Publication Interface', () => {
    it('should define the properties of a Publication object', () => {
        const publication: Publication = {
            doi: 'sample-doi',
            pubmed_id: 'sample-pmid',
            pubmed_cid: 'sample-pmcid',
            title: 'Sample Title',
            apa_html: 'APA HTML',
            reference_html: 'Reference HTML',
        };

        expect(publication).toEqual({
            doi: 'sample-doi',
            pubmed_id: 'sample-pmid',
            pubmed_cid: 'sample-pmcid',
            title: 'Sample Title',
            apa_html: 'APA HTML',
            reference_html: 'Reference HTML',
        });
    });
});

describe('LiteratureServiceResponse Interface', () => {
    it('should define the properties of a LiteratureServiceResponse object', () => {
        const response: LiteratureServiceResponse = {
            status: LiteratureServiceResponseStatusEnum.Success,
            publication: {
                doi: 'sample-doi',
                pubmed_id: 'sample-pmid',
                pubmed_cid: 'sample-pmcid',
                title: 'Sample Title',
                apa_html: 'APA HTML',
                reference_html: 'Reference HTML',
            },
            error: 'Sample Error',
        };

        expect(response).toEqual({
            status: LiteratureServiceResponseStatusEnum.Success,
            publication: {
                doi: 'sample-doi',
                pubmed_id: 'sample-pmid',
                pubmed_cid: 'sample-pmcid',
                title: 'Sample Title',
                apa_html: 'APA HTML',
                reference_html: 'Reference HTML',
            },
            error: 'Sample Error',
        });
    });
});

describe('LiteratureServiceResponseStatusEnum', () => {
    it('should define the possible status values', () => {
        expect(LiteratureServiceResponseStatusEnum.Success).toBe(0);
        expect(LiteratureServiceResponseStatusEnum.NoResult).toBe(1);
        expect(LiteratureServiceResponseStatusEnum.ApiError).toBe(2);
        expect(LiteratureServiceResponseStatusEnum.UnknownError).toBe(3);
    });
});