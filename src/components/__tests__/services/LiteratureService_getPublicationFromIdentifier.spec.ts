import { expect, describe, test, afterAll } from "vitest";

// core test 
import { //
    getPublicationFromIdentifier,//
    LiteratureServiceResponse,//
    LiteratureServiceResponseStatusEnum,//
} from '@/services/LiteratureService'

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const defaultResponse = {
    "version": "6.9",
    "hitCount": 0,
    "request": {
        "queryString": "XXX",
        "resultType": "lite",
        "cursorMark": "*",
        "pageSize": 25,
        "sort": "",
        "synonym": false
    },
    "resultList": {
        "result": [] as any[]
    }
};

const serverOK1 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        const okJson = defaultResponse;
        okJson.hitCount = 1;
        okJson.request.queryString = "mock1";
        okJson.resultList.result = [];
        okJson.resultList.result.push({
            pubYear: '2023',
            title: 'Sample Title',
            journalTitle: 'Sample Journal',
            authorString: 'John Doe, Jane Smith, Alice Johnson',
            journalVolume: '5',
            issue: '2',
            doi: 'doi:sample-doi',
            pmid: 'sample-pmid',
            pmcid: 'sample-pmcid',
        });
        return res(
            ctx.json(okJson),
        )
    }),
);

const serverKO1 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        return res(ctx.status(500), ctx.json({}), ctx.text('xxx'))
    }),
);

const serverKO2 = setupServer(
    rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
        return res(ctx.status(222), ctx.json({}), ctx.text('xxx'))
    }),
);

afterAll(() => {
    // Clean up after all tests are done, preventing this
    // interception layer from affecting irrelevant tests.
    serverOK1.close();
    serverKO1.close();
    serverKO2.close();
})

describe('getPublicationFromIdentifier function', () => {
    test('should return a correctly processed LiteratureServiceResponse (success)', async () => {
        serverOK1.listen()
        await getPublicationFromIdentifier("mock1").then((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication.apa_html).toBe('John, Doe., Jane, Smith. &amp; Alice, Johnson (2023). Sample Title <em>Sample Journal</em>, 5(2). doi:sample-doi.');
            expect(lsr.publication.reference_html).toBe('John <em>et al.</em> 2023');
            expect(lsr.publication.doi).toBe('doi:sample-doi');
            expect(lsr.publication.pubmed_id).toBe('sample-pmid');
            expect(lsr.publication.pubmed_cid).toBe('sample-pmcid');
        });
        serverOK1.close();
    });

    test('should return an error - 500 (reject)', async () => {
        serverKO1.listen()
        await getPublicationFromIdentifier("mock_ko1").catch((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication).toBe(undefined);
            expect(lsr.status).toBe(LiteratureServiceResponseStatusEnum.UnknownError);
            expect(lsr.error).toBe("500 | Internal Server Error");
        });
        serverKO1.close();
    });

    test('should return an error - 300 (reject)', async () => {
        serverKO2.listen()
        await getPublicationFromIdentifier("mock_ko2").catch((lsr: LiteratureServiceResponse) => {
            expect(lsr.publication).toBe(undefined);
            expect(lsr.status).toBe(LiteratureServiceResponseStatusEnum.UnknownError);
            expect(lsr.error).toBe("222 | OK");
        });
        serverKO2.close();
    });

});
