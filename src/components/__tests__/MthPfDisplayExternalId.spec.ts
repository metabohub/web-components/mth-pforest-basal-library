import { mount } from '@vue/test-utils';
import { expect, test } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfDisplayExternalId from '../MthPfDisplayExternalId.vue';

const global = {
  plugins: [vuetify]
}

// let open_url = ""
// Object.defineProperty(window, "open", {
//   value: (url) => { open_url = url }
// });

// let clipboardContents = "";
// Object.defineProperty(navigator, "clipboard", {
//   value: {
//     writeText: async (text: string) => {
//       clipboardContents = text;
//     },
//     readText: () => clipboardContents,
//   },
// });

// test basic
test('component exists', async () => {
  // mount
  expect(MthPfDisplayExternalId).toBeTruthy();
});

// TEST ATTRIBUTES
test('mount component - option - identifierLabel', async () => {
  // mount
  const wrapper = mount(MthPfDisplayExternalId, { global, props: { "identifier-label": "initial" } });
  // check initial data
  expect(wrapper.vm.label).toBe('initial');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ identifierLabel: 'changed' });
  // check after
  expect(wrapper.vm.label).toBe('changed');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - identifierPrefix', async () => {
  // mount
  const wrapper = mount(MthPfDisplayExternalId, { global, props: { "identifier-prefix": "initial" } });
  // check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('initial');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ identifierPrefix: 'changed' });
  // check after
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('changed');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - identifierSuffixLenght', async () => {
  // mount
  const wrapper = mount(MthPfDisplayExternalId, { global, props: { "identifier-suffix-lenght": 8 } });
  // check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(8);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ identifierSuffixLenght: 42 });
  // check after
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(42);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - identifierValue', async () => {
  // mount
  const wrapper = mount(MthPfDisplayExternalId, { global, props: { "identifier-value": '123' } });
  // check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('123');
  expect(wrapper.vm.url).toBe('123');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('123');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ identifierValue: '456' });
  // check after
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('456');
  expect(wrapper.vm.url).toBe('456');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('456');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - identifierUrlPrefix', async () => {
  // mount
  const wrapper = mount(MthPfDisplayExternalId, { global, props: { "identifier-url-prefix": 'initial' } });
  // check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('initial');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ identifierUrlPrefix: 'changed!' });
  // check after
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('changed!');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - isReadonly', async () => {
  // mount
  const wrapper = mount(MthPfDisplayExternalId, { global, props: { "is-readonly": false } });
  // check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(false);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ isReadonly: true });
  // check after
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - identifierPlaceholder', async () => {
  // mount
  const wrapper = mount(MthPfDisplayExternalId, { global, props: { "identifier-placeholder": 'initial' } });
  // check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('initial');
  // set props
  await wrapper.setProps({ identifierPlaceholder: 'changed' });
  // check after
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.prefix).toBe('');
  expect(wrapper.vm.suffixLength).toBe(0);
  expect(wrapper.vm.urlPrefix).toBe('');
  expect(wrapper.vm.hiddenVal).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('changed');
});

// // TEST BUTTONS
// test('test - openUrl', async () => {
//   // init
//   open_url = '';
//   const wrapper = mount(MthPfDisplayExternalId, { global });
//   wrapper.vm.urlPrefix = 'urlPrefix:';
//   wrapper.vm.suffixLength = 20;
//   wrapper.vm.value = '+value';
//   wrapper.vm.hiddenVal = '+hiddenVal';
//   // build url
//   wrapper.vm.updateUrl();
//   // trigger
//   const button = wrapper.find('.fa-link'); 
//   await button.trigger('click');
//   // check
//   expect(open_url).toBe('');
//   // reset
//   open_url = '';
// });

// test('test - copyToClipboard', async () => {
//   // init
//   clipboardContents = '';
//   const wrapper = mount(MthPfDisplayExternalId, { global });
//   wrapper.vm.prefix = 'prefix:';
//   wrapper.vm.suffixLength = 20;
//   wrapper.vm.value = '+value';
//   wrapper.vm.hiddenVal = '+hiddenVal';
//   // build 
//   wrapper.vm.formatValue();
//   // trigger
//   const button = wrapper.find('.fa-clipboard');
//   await button.trigger('click');
//   // check
//   expect(clipboardContents).toBe('');
//   // reset
//   clipboardContents = '';
// });

// TEST METHODS

test('test - focusIn', async () => {
  // init
  const wrapper = mount(MthPfDisplayExternalId, { global });
  // check mode readonly
  // check mode readonly - init
  await wrapper.setProps({ isReadonly: true });
  wrapper.vm.value = 'value';
  wrapper.vm.hiddenVal = 'hiddenVal';
  // check mode readonly - call method
  wrapper.vm.focusIn();
  // check mode readonly - check
  expect(wrapper.vm.value).toBe('value');
  expect(wrapper.vm.hiddenVal).toBe('hiddenVal');
  // check mode edit
  // check mode edit - init
  await wrapper.setProps({ isReadonly: false });
  wrapper.vm.value = 'value';
  wrapper.vm.hiddenVal = 'hiddenVal';
  // check mode edit - call method
  wrapper.vm.focusIn();
  // check mode edit - check
  expect(wrapper.vm.value).toBe('hiddenVal');
  expect(wrapper.vm.hiddenVal).toBe('hiddenVal');
});

test('test - focusOut', async () => {
  // mount
  const wrapper = mount(MthPfDisplayExternalId, { global });
  // init
  wrapper.vm.prefix = 'prefix:';
  wrapper.vm.suffixLength = 15;
  wrapper.vm.value = '+value';
  wrapper.vm.hiddenVal = '+hiddenVal';
  //  call method
  wrapper.vm.focusOut();
  //  check
  expect(wrapper.vm.value).toBe('prefix:00000+hiddenVal');
  expect(wrapper.vm.hiddenVal).toBe('+hiddenVal');
});

test('test - formatValue', async () => {
  // mount
  const wrapper = mount(MthPfDisplayExternalId, { global });
  // init
  wrapper.vm.prefix = 'prefix:';
  wrapper.vm.suffixLength = 15;
  wrapper.vm.value = '+value';
  wrapper.vm.hiddenVal = '+hiddenVal';
  //  call method
  wrapper.vm.formatValue();
  //  check
  expect(wrapper.vm.value).toBe('prefix:00000+hiddenVal');
  expect(wrapper.vm.hiddenVal).toBe('+hiddenVal');
});

test('test - updateUrl - classic', async () => {
  // mount
  const wrapper = mount(MthPfDisplayExternalId, { global });
  // init
  wrapper.vm.urlPrefix = 'urlPrefix:';
  wrapper.vm.hiddenVal = '+hiddenVal';
  //  call method
  wrapper.vm.updateUrl();
  //  check
  expect(wrapper.vm.url).toBe('urlPrefix:+hiddenVal');
});

test('test - updateUrl void', async () => {
  // init
  const wrapper = mount(MthPfDisplayExternalId, { global });
  // init
  wrapper.vm.urlPrefix = 'urlPrefix:';
  wrapper.vm.hiddenVal = '';
  //  call method
  wrapper.vm.updateUrl();
  //  check
  expect(wrapper.vm.url).toBe('');
});

test('test - triggerUpdatePropValue void', async () => {
  // mount
  const wrapper = mount(MthPfDisplayExternalId, { global });
  // init
  wrapper.vm.value = 'initial';
  //  call method
  wrapper.vm.triggerUpdatePropValue("newValue");
  //  check
  expect(wrapper.vm.value).toBe('newValue');
});

test('test - cleanHiddenValue', async () => {
  // init
  const wrapper = mount(MthPfDisplayExternalId, { global });
  // check mode suffixLength=0
  // check mode suffixLength=0 - init
  wrapper.vm.prefix = 'prefix:';
  wrapper.vm.suffixLength = 0;
  wrapper.vm.value = 'prefix:0000+value';
  wrapper.vm.hiddenVal = 'hiddenVal';
  // check mode suffixLength=0 - call method
  wrapper.vm.cleanHiddenValue();
  // check mode suffixLength=0 - check
  expect(wrapper.vm.value).toBe('prefix:0000+value');
  expect(wrapper.vm.hiddenVal).toBe('0000+value');
  // check mode suffixLength>0
  // check mode ssuffixLength>0 - init
  wrapper.vm.prefix = 'prefix:';
  wrapper.vm.suffixLength = 1;
  wrapper.vm.value = 'prefix:0000+value';
  wrapper.vm.hiddenVal = 'hiddenVal';
  // check mode suffixLength>0 - call method
  wrapper.vm.cleanHiddenValue();
  // check mode suffixLength>0 - check
  expect(wrapper.vm.value).toBe('prefix:0000+value');
  expect(wrapper.vm.hiddenVal).toBe('+value');
});

