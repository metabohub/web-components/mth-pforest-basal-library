import { mount } from '@vue/test-utils';
import { expect, test } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfDisplayLink from '../MthPfDisplayLink.vue';

const global = {
  plugins: [vuetify]
}

let open_url = ""
Object.defineProperty(window, "open", {
  value: (url) => { open_url = url }
});

test('component exists', async () => {
  expect(MthPfDisplayLink).toBeTruthy();
});

test('mount component - option - linkLabel', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayLink, { global, props: { linkLabel: "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('set by parent');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ linkLabel: 'changed!' });
  expect(wrapper.vm.label).toBe('changed!');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - link-label', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayLink, { global, props: { "link-label": "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('set by parent');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ 'linkLabel': 'changed!' });
  expect(wrapper.vm.label).toBe('changed!');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - linkValue', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayLink, { global, props: { linkValue: "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('set by parent');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ linkValue: 'changed!' });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('changed!');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - link-value', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayLink, { global, props: { "link-value": "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('set by parent');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ 'linkValue': 'changed!' });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('changed!');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set to void - test if take URL value
  await wrapper.setProps({ 'linkUrl': 'https://toto.com' });
  await wrapper.setProps({ 'linkValue': undefined });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('https://toto.com');
  expect(wrapper.vm.url).toBe('https://toto.com');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set to void - test if take URL value
  await wrapper.setProps({ 'linkUrl': 'https://titi.com' });
  await wrapper.setProps({ 'linkValue': '' });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('https://titi.com');
  expect(wrapper.vm.url).toBe('https://titi.com');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - linkUrl', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayLink, { global, props: { linkUrl: "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('set by parent');
  expect(wrapper.vm.url).toBe('set by parent');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ 'linkUrl': 'changed!' });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('changed!');
  expect(wrapper.vm.url).toBe('changed!');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - link-url', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayLink, { global, props: { "link-url": "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('set by parent');
  expect(wrapper.vm.url).toBe('set by parent');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ 'linkUrl': 'changed!' });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('changed!');
  expect(wrapper.vm.url).toBe('changed!');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('test - openUrl', async () => {
  open_url = '';
  const wrapper = mount(MthPfDisplayLink, { global });
  wrapper.vm.url = 'http://example1.com'
  wrapper.vm.openUrl()
  expect(open_url).toBe('http://example1.com');
  open_url = '';
})

test('test - openUrl (no url)', async () => {
  open_url = '';
  const wrapper = mount(MthPfDisplayLink, { global });
  wrapper.vm.value = 'http://example2.com';
  wrapper.vm.url = null;
  wrapper.vm.openUrl();
  expect(open_url).toBe('http://example2.com');
  open_url = '';
})

// edit mode

test('mount component - option - isReadonly', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayLink, { global, props: { isReadonly: false } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(false);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ isReadonly: true });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - link-placeholder', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayLink, { global, props: { linkPlaceholder: 'initial' } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('initial');
  // set props
  await wrapper.setProps({ linkPlaceholder: 'updated' });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.url).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('updated');
});