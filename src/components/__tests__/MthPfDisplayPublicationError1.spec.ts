import { mount } from '@vue/test-utils';
import { afterAll, expect, test } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfDisplayPublication from '@/components/MthPfDisplayPublication.vue';
import { Status } from "@/components/utils";

const global = {
  plugins: [vuetify]
}

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const serverKO = setupServer(//
  //- ///europepmc/webservices/rest/searchquery=mock1&format=json&domain=literature
  rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
    return res(ctx.status(500), ctx.json({}), ctx.text('xxx'))
  }),
);

afterAll(() => {
  // Clean up after all tests are done, preventing this
  // interception layer from affecting irrelevant tests.
  serverKO.close();
})

test('mount component - method loadPublicationDataFromIdentifier', async () => {
  // mock
  serverKO.listen();
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x' } });
  // Call the method to load data
  await sleep(50);
  // Ensure that the status and publication properties are updated correctly
  expect(wrapper.vm.status).toBe(Status.Error);
  expect(wrapper.vm.publication).toEqual({});
  expect(wrapper.vm.errorMessage).toEqual("An unknown error occurred.");

});

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}