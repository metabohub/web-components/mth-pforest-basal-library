import { mount } from '@vue/test-utils';
import { expect, test } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfDisplayScoredValue from '@/components/MthPfDisplayScoredValue.vue';

const global = {
    plugins: [vuetify]
}

test('component exits', async () => {
    expect(MthPfDisplayScoredValue).toBeTruthy();
});

test('renders the component with default props', () => {
    const wrapper = mount(MthPfDisplayScoredValue,
        {
            global,
            props: {
                scoreValue: 0,
                nbStars: 1,
            }
        });
    // Init state of the component with default props
    expect(wrapper.props('scoreValue')).toBe(0);
    expect(wrapper.props('nbStars')).toBe(1);
    expect(wrapper.props('showChip')).toBe(false);
    expect(wrapper.props('isReadonly')).toBe(true);
    expect(wrapper.vm.score).toBe(0);
    expect(wrapper.vm.starsCount).toBe(1);
    // Check if v-rating exist
    const ratingComponent = wrapper.findComponent({ name: 'VRating' });
    expect(ratingComponent.exists()).toBe(true);
    // Check if v-chip exist
    const chipComponent = wrapper.findComponent({ name: 'VChip' });
    expect(chipComponent.exists()).toBe(false);
});

test('updates the score when scoreValue prop changes', async () => {
    const wrapper = mount(MthPfDisplayScoredValue, {
        global,
        props: {
            scoreValue: 0,
            nbStars: 1,
        }
    });
    expect(wrapper.vm.score).toBe(0);
    // Update the scoreValue prop and wait for the component to re-render
    await wrapper.setProps({ scoreValue: 4 });
    // 'score' ref has been updated
    expect(wrapper.vm.score).toBe(4);
});

test('updates the starsCount when nbStars prop changes', async () => {
    const wrapper = mount(MthPfDisplayScoredValue, {
        global,
        props: {
            scoreValue: 0,
            nbStars: 1,
        }
    });
    expect(wrapper.vm.starsCount).toBe(1);
    // Update the nbStars prop and wait for the component to re-render
    await wrapper.setProps({ nbStars: 10 });
    // 'starsCount' ref has been updated
    expect(wrapper.vm.starsCount).toBe(10);
});

test('shows v-chip when showChip is true', async () => {
    const wrapper = mount(MthPfDisplayScoredValue, {
        global,
        props: {
            scoreValue: 0,
            nbStars: 3,
        }
    });
    // Set the showChip prop to true
    await wrapper.setProps({ showChip: true });
    // Find the v-chip component
    const chipComponent = wrapper.findComponent({ name: 'VChip' });
    // Assert that the v-chip is visible
    expect(chipComponent.exists()).toBe(true);
    // Assert the content of the v-chip
    expect(chipComponent.text()).toBe('0');
    // Update the score ref and check if v-chip content is updated
    await wrapper.setProps({ 'scoreValue': 1 });
    expect(chipComponent.text()).toBe('3');
});

test('renders in readonly mode when isReadonly prop is true', () => {
    const wrapper = mount(MthPfDisplayScoredValue, {
        global,
        props: {
            scoreValue: 0.5,
            nbStars: 4,
            isReadonly: true
        }
    });
    // Find the readonly v-rating component
    const ratingComponent = wrapper.findComponent({ name: 'VRating' });
    // Assert that the v-rating is visible
    expect(ratingComponent.exists()).toBe(true);
    // Find the v-chip component
    const chipComponent = wrapper.findComponent({ name: 'VChip' });
    // Assert that the v-chip is visible
    expect(chipComponent.exists()).toBe(false);
});

test('renders in editable mode when isReadonly prop is false', () => {
    const wrapper = mount(MthPfDisplayScoredValue, {
        global,
        props: {
            scoreValue: 0.5,
            nbStars: 4,
            isReadonly: false
        }
    });
    // Find the editable v-rating component
    const ratingComponent = wrapper.findComponent({ name: 'VRating' });
    // Assert that the v-rating is visible
    expect(ratingComponent.exists()).toBe(true);
    // Find the v-chip component
    const chipComponent = wrapper.findComponent({ name: 'VChip' });
    // Assert that the v-chip is visible
    expect(chipComponent.exists()).toBe(false);
});

test('mode readonly - set score', async () => {
    // init
    const wrapper = mount(MthPfDisplayScoredValue, {
        global,
        props: {
            scoreValue: 0.5,
            isReadonly: true,
            showChip: true,
            nbStars: 1,
        }
    });
    // check before
    expect(wrapper.vm.score).toBe(0.5);
    // Set the showChip prop to true
    await wrapper.setProps({ 'scoreValue': 1 });
    // check
    expect(wrapper.vm.score).toBe(1);
});

test('mode edit - set score', async () => {
    // init
    const wrapper = mount(MthPfDisplayScoredValue, {
        global,
        props: {
            scoreValue: 0.5,
            isReadonly: false,
            showChip: true,
            nbStars: 1,
        }
    });
    // check before
    expect(wrapper.vm.score).toBe(0.5);
    // Set the showChip prop to true
    await wrapper.setProps({ 'scoreValue': 1 });
    // check
    expect(wrapper.vm.score).toBe(1);
});

test('set mode edit', async () => {
    // init
    const wrapper = mount(MthPfDisplayScoredValue, {
        global,
        props: {
            scoreValue: 0.5,
            isReadonly: false,
            showChip: true,
            nbStars: 1,
        }
    });
    // check before
    expect(wrapper.vm.modeReadonly).toBe(false);
    // Set the showChip prop to true
    await wrapper.setProps({ 'isReadonly': true });
    // check
    expect(wrapper.vm.modeReadonly).toBe(true);
});