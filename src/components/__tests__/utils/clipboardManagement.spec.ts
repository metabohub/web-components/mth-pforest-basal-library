import { expect, test, } from "vitest";

// core test
import { copyPropertyValueToClipboard } from '@/components/utils';


let clipboardContents = "";
Object.defineProperty(navigator, "clipboard", {
    value: {
        writeText: async (text: string) => {
            clipboardContents = text;
        },
        readText: () => clipboardContents,
    },
});

test('test method: copyPropertyValueToClipboard', async () => {
    // init
    clipboardContents = "";
    // Check initial values 
    // test clipboard before
    expect(clipboardContents).to.eq("");
    // init elemt
    const elem = new HTMLInputElement();
    elem.value = "test copyPropertyValueToClipboard";
    // call method 'copyToClipboard'
    await copyPropertyValueToClipboard(elem);
    // check clipboard
    expect(clipboardContents).to.eq("test copyPropertyValueToClipboard");
    //===================
    // re-init, then test with an bad init, then check clipboard 
    clipboardContents = "";
    const elemFail = new HTMLInputElement();
    elemFail.removeAttribute("value");
    await  copyPropertyValueToClipboard(elemFail);
    expect(clipboardContents).to.eq("");
});

