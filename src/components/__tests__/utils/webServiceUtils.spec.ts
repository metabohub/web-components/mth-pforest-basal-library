import { expect, describe, it } from "vitest";

// core test
import { Status } from '@/components/utils';

// tests!
describe('Status Enum', () => {
    it('should have the correct values', () => {
        expect(Status.Init).toBe(0);
        expect(Status.Loading).toBe(1);
        expect(Status.Processing).toBe(2);
        expect(Status.Success).toBe(3);
        expect(Status.Error).toBe(4);
    });

    it('should have unique values', () => {
        const values = Object.values(Status);
        const uniqueValues = new Set(values);
        expect(uniqueValues.size).toBe(values.length);
    });
});
