import { mount } from '@vue/test-utils';
import { expect, test, } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfDisplayProperty, { PropertyTypeEnum } from '@/components/MthPfDisplayProperty.vue';


const global = {
  plugins: [vuetify]
}

let clipboardContents = "";
Object.defineProperty(navigator, "clipboard", {
  value: {
    writeText: async (text: string) => {
      clipboardContents = text;
    },
    readText: () => clipboardContents,
  },
});

test('component exists', async () => {
  expect(MthPfDisplayProperty).toBeTruthy();
});

test('mount component - no option', async () => {
  const wrapper = mount(MthPfDisplayProperty, { global });
  // Check initial icon value
  expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard');
});

test('test method: copyToClipboard - OK', async () => {
  // init
  clipboardContents = "";
  const wrapper = mount(MthPfDisplayProperty, { global });
  // Check initial icon value
  expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard');
  // test clipboard before
  expect(clipboardContents).to.eq("");
  await wrapper.setProps({ 'propertyValue': 'test copy' });
  // call method 'copyToClipboard'
  await wrapper.vm.copyToClipboard();
  // Check if copied new icon value
  expect(clipboardContents).to.eq("test copy");
  expect(wrapper.vm.appendIcon).toBe('fas fa-circle-check fa-bounce');
  // check re-reset after 3 seconds
  await setTimeout(function () {
    expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard')
  }, 3000);
});

test('test method: copyToClipboard - KO', async () => {
  // init
  clipboardContents = "";
  const wrapper = mount(MthPfDisplayProperty,//
    {
      global,
      props: { 'property-value': 'test copy' },
      setup(props) {
        return {
          inputRef: null,
          color: "",
          showMessage: false,
          message: "",
          label: "",
          value: props.propertyValue,
          type: props.propertyType,
          prependIcon: "",
          isClicked: false,
          hasSlotInTextField: false,
          //
          modeReadonly: false,
          placeholder: "",
        }
      }
    });
  // test clipboard before
  expect(clipboardContents).to.eq("");
  // Check initial values
  expect(wrapper.vm.value).toBe('test copy');
  // call method 'copyToClipboard'
  await wrapper.vm.copyToClipboard();
  // Check clipboard after
  expect(clipboardContents).to.eq("test copy");
  // Check if icon has been updated
  expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard');
});

test('test method: hideSuccessCopyMessage', async () => {
  // init 
  const wrapper = mount(MthPfDisplayProperty, { global });
  // change initial values
  wrapper.vm.color = "changed";
  wrapper.vm.message = "changed";
  wrapper.vm.showMessage = false;
  expect(wrapper.vm.color).toBe('changed');
  expect(wrapper.vm.message).toBe('changed');
  expect(wrapper.vm.showMessage).toBe(false);
  // call method
  wrapper.vm.hideSuccessCopyMessage();
  // Check new values
  expect(wrapper.vm.color).toBe('');
  expect(wrapper.vm.message).toBe('');
  expect(wrapper.vm.showMessage).toBe(false);
});

test('test method: removeColor', async () => {
  // init 
  const wrapper = mount(MthPfDisplayProperty, { global });
  // change initial values
  wrapper.vm.isClicked = false;
  expect(wrapper.vm.isClicked).toBe(false);
  // call method
  wrapper.vm.removeColor();
  // Check new values
  expect(wrapper.vm.showMessage).toBe(false);
});

test('mount component - option - propertyLabel', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayProperty, { global, props: { propertyLabel: "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('set by parent');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('');
  // set props
  await wrapper.setProps({ propertyLabel: 'changed!' });
  expect(wrapper.vm.label).toBe('changed!');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('');
});

// new pforest#102
test('mount component - option - propertyLabel HTML', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayProperty, { global, props: { propertyLabel: "set by <em>parent<em>" } });
  // Check initial data
  //expect(wrapper.vm.label).toBe('set by parent');
  expect(wrapper.vm.label).toContain('set by <em>parent<em>');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('');
  // set props
  await wrapper.setProps({ propertyLabel: '<b>changed!<b>' });
  expect(wrapper.vm.label).toBe('<b>changed!<b>');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('');
});

test('mount component - option - property-label', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayProperty, { global, props: { "property-label": "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('set by parent');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('');
  // set props
  await wrapper.setProps({ 'propertyLabel': 'changed!' });
  expect(wrapper.vm.label).toBe('changed!');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('');
});

test('mount component - option - propertyValue', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayProperty, { global, props: { propertyValue: "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('set by parent');
  expect(wrapper.vm.prependIcon).toBe('');
  // set props
  await wrapper.setProps({ propertyValue: 'changed!' });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('changed!');
  expect(wrapper.vm.prependIcon).toBe('');
});

test('mount component - option - property-value', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayProperty, { global, props: { "property-value": "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('set by parent');
  expect(wrapper.vm.prependIcon).toBe('');
  // set props
  await wrapper.setProps({ 'propertyValue': 'changed!' });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('changed!');
  expect(wrapper.vm.prependIcon).toBe('');
});

test('mount component - option - propertyIcon', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayProperty, { global, props: { propertyIcon: "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('set by parent');
  expect(wrapper.vm.modeReadonly).toBe(true);
  // set props
  await wrapper.setProps({ propertyIcon: 'changed!' });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('changed!');
});

test('mount component - option - property-icon', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayProperty, { global, props: { "property-icon": "set by parent" } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('set by parent');
  expect(wrapper.vm.modeReadonly).toBe(true);
  // set props
  await wrapper.setProps({ 'propertyIcon': 'changed!' });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('changed!');
});

test('test method: restorInitialDisplay', async () => {
  // init 
  const wrapper = mount(MthPfDisplayProperty, { global });
  // change initial values
  wrapper.vm.showMessage = true;
  expect(wrapper.vm.showMessage).toBe(true);
  // call method
  await wrapper.vm.restorInitialDisplay(0);
  // Check new values
  expect(wrapper.vm.showMessage).toBe(false);
});

test('test method: copyToClipboard - v-slot', async () => {
  // init
  clipboardContents = "";
  const wrapper = mount(MthPfDisplayProperty, { global, slots: { "in-text-field": "set by parent" } });
  // Check initial icon value
  expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard');
  // test clipboard before
  expect(clipboardContents).to.eq("");
  await wrapper.setProps({ 'propertyValue': 'test copy' });
  // call method 'copyToClipboard'
  await wrapper.vm.copyToClipboard();
  // Check if copied new icon value
  expect(clipboardContents).to.eq("test copy");
  expect(wrapper.vm.appendIcon).toBe('fas fa-circle-check fa-bounce');
  // check re-reset after 3 seconds
  await setTimeout(function () {
    expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard')
  }, 3000);
});

test('test method: copyToClipboard - v-slot', async () => {
  // init
  clipboardContents = "";
  const wrapper = mount(MthPfDisplayProperty, { global, slots: { "prepend": "set by parent" } });
  // Check initial icon value
  expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard');
  // test clipboard before
  expect(clipboardContents).to.eq("");
  await wrapper.setProps({ 'propertyValue': 'test copy' });
  // call method 'copyToClipboard'
  await wrapper.vm.copyToClipboard();
  // Check if copied new icon value
  expect(clipboardContents).to.eq("test copy");
  expect(wrapper.vm.appendIcon).toBe('fas fa-circle-check fa-bounce');
  // check re-reset after 3 seconds
  await setTimeout(function () {
    expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard')
  }, 3000);
});

// MODE EDITION

test('mount component - option - is-readonly', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayProperty, { global, props: { "is-readonly": false } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(false);
  expect(wrapper.vm.placeholder).toBe('');
  // set props
  await wrapper.setProps({ 'isReadonly': true });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('');
});

test('mount component - option - property-placeholder', async () => {
  // mount 
  const wrapper = mount(MthPfDisplayProperty, { global, props: { "property-placeholder": 'inital placeholder' } });
  // Check initial data
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('inital placeholder');
  // set props
  await wrapper.setProps({ 'propertyPlaceholder': 'updated placeholder' });
  expect(wrapper.vm.label).toBe('');
  expect(wrapper.vm.value).toBe('');
  expect(wrapper.vm.prependIcon).toBe('');
  expect(wrapper.vm.modeReadonly).toBe(true);
  expect(wrapper.vm.placeholder).toBe('updated placeholder');
});

test('test method: copyToClipboardFromInput - mode readonly (default)', async () => {
  // init
  clipboardContents = "";
  const wrapper = mount(MthPfDisplayProperty, { global });
  // Check initial icon value
  expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard');
  // test clipboard before
  expect(clipboardContents).to.eq("");
  await wrapper.setProps({ 'propertyValue': 'test copy' });
  // call method 'copyToClipboardFromInput'
  await wrapper.vm.copyToClipboardFromInput();
  // Check if copied new icon value
  expect(clipboardContents).to.eq("test copy");
  expect(wrapper.vm.appendIcon).toBe('fas fa-circle-check fa-bounce');
  // check re-reset after 3 seconds
  await setTimeout(function () {
    expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard')
  }, 3000);
});

test('test method: copyToClipboardFromInput - mode edition', async () => {
  // init
  clipboardContents = "";
  const wrapper = mount(MthPfDisplayProperty, { global });
  // Check initial icon value
  expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard');
  // test clipboard before
  expect(clipboardContents).to.eq("");
  await wrapper.setProps({ 'propertyValue': 'test copy', 'isReadonly': false });
  // call method 'copyToClipboardFromInput'
  await wrapper.vm.copyToClipboardFromInput();
  // Check if copied new icon value
  expect(clipboardContents).to.eq("");
  expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard');
});

test('test prop: textarea', async () => {
  // init
  clipboardContents = "";
  const wrapper = mount(MthPfDisplayProperty, { global });
  // Check initial icon value
  expect(wrapper.vm.appendIcon).toBe('fas fa-clipboard');
  // test clipboard before
  expect(clipboardContents).to.eq("");
  await wrapper.setProps({ 'propertyValue': `line 1\nline 2`, 'propertyType': PropertyTypeEnum.Textarea });
  // call method 'copyToClipboardFromInput'
  await wrapper.vm.copyToClipboardFromInput();
  // Check if copied new icon value
  expect(clipboardContents).to.eq("line 1\nline 2");
  expect(wrapper.vm.appendIcon).toBe('fas fa-circle-check fa-bounce');
});