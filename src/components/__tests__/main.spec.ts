import { createApp } from 'vue';
import { MetabohubPeakForestBasalLibrary } from '@/components/main';
import { describe, it } from 'vitest';

describe('MetabohubPeakForestBasalLibrary', () => {
    it('should register MthPfDisplayProperty component', () => {
        const app = createApp({});
        MetabohubPeakForestBasalLibrary.install(app);
    });
});