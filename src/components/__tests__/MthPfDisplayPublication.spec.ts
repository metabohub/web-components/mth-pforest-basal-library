import { mount } from '@vue/test-utils';
import { afterAll, expect, test } from "vitest";
import vuetify from '@/plugins/vuetify';

// core test
import MthPfDisplayPublication from '@/components/MthPfDisplayPublication.vue';
import { Status } from "@/components/utils";
import { LiteratureServiceResponse, LiteratureServiceResponseStatusEnum } from "@/services/LiteratureService";

const global = {
  plugins: [vuetify]
}

let clipboardContents = "";
Object.defineProperty(navigator, "clipboard", {
  value: {
    writeText: async (text: string) => {
      clipboardContents = text;
    },
    readText: () => clipboardContents,
  },
});

// mock
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const defaultResponse = {
  "version": "6.9",
  "hitCount": 0,
  "request": {
    "queryString": "XXX",
    "resultType": "lite",
    "cursorMark": "*",
    "pageSize": 25,
    "sort": "",
    "synonym": false
  },
  "resultList": {
    "result": [] as any[]
  }
};

const serverOK = setupServer(//
  //- ///europepmc/webservices/rest/searchquery=mock1&format=json&domain=literature
  rest.get('*/europepmc/webservices/rest/*', (req, res, ctx) => {
    const okJson = defaultResponse;
    okJson.hitCount = 1;
    okJson.request.queryString = "mock1";
    okJson.resultList.result = [];
    okJson.resultList.result.push({
      authorString: '',
    });
    return res(
      ctx.json(okJson),
    )
  }),
);

afterAll(() => {
  // Clean up after all tests are done, preventing this
  // interception layer from affecting irrelevant tests.
  serverOK.close();
})


test('component exists', async () => {
  // mount
  expect(MthPfDisplayPublication).toBeTruthy();
});

test('mount component - just mandatory option', async () => {
  // mock
  serverOK.listen();
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x' } });
  // Check initial values
  expect(wrapper.vm.publicationIdentifier).toBe('x');
  expect(wrapper.vm.isReadonly).toBe(true);
  // Check default text
  expect(wrapper.text()).toBe("initialization:  x");
  // set mandatroy option
  await wrapper.setProps({ publicationIdentifier: 'y' });
  await (wrapper.vm.status = Status.Init)
  expect(wrapper.text()).toBe("initialization:  y");
});

test('mount component - texts', async () => {
  // mock
  serverOK.listen();
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x' } });
  // Check initial values
  await (wrapper.vm.loadPublicationDataFromIdentifier = () => { });
  await (sleep(50))
  expect(wrapper.vm.status).toBe(Status.Success);
  // set values for all test
  await (wrapper.vm.errorMessage = 'Error Message')
  await (wrapper.vm.publication = {
    apa_html: "apa",
    doi: "doi",
    reference_html: "ref",
    pubmed_cid: "pmcid",
    pubmed_id: "pmid",
    title: "title",
  })
  await (wrapper.vm.loadingValue = -33)
  // set 1
  await (wrapper.vm.status = Status.Init)
  expect(wrapper.text()).toBe("initialization:  x");
  // set 2
  await (wrapper.vm.status = Status.Loading)
  expect(wrapper.text()).toBe("-33");
  // set 3
  await (wrapper.vm.status = Status.Processing)
  expect(wrapper.text()).toBe("");
  // set 4
  await (wrapper.vm.publication.reference_html = "___REF___")
  await (wrapper.vm.status = Status.Success)
  expect(wrapper.text()).toContain("___REF___");
  // set 5
  await (wrapper.vm.status = Status.Error)
  expect(wrapper.text()).toBe("Error Message");
});

test('mount component - option is-readonly', async () => {
  // mock
  serverOK.listen();
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x' } });
  // set values
  await (wrapper.vm.status = Status.Success)
  await (wrapper.vm.publication = {
    apa_html: "apa",
    doi: "doi",
    reference_html: "ref",
    pubmed_cid: "pmcid",
    pubmed_id: "pmid",
    title: "title",
  })
  // Check initial values
  expect(wrapper.vm.publicationIdentifier).toBe('x');
  expect(wrapper.vm.isReadonly).toBe(true);
  // Check default text
  expect(wrapper.html()).not.toContain("fa-trash"); //
  // set mandatroy option
  await wrapper.setProps({ isReadonly: false });
  expect(wrapper.html()).toContain("fa-trash"); // ;
});

test('mount component - method runProgressProgress', async () => {
  // mock
  serverOK.listen();
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x' } });
  // set values
  await (wrapper.vm.loadingValue = 100);
  expect(wrapper.vm.loadingValue).toBe(100);
  // run test 1
  await (wrapper.vm.runProgressProgress());
  expect(wrapper.vm.loadingValue).not.toBe(100);
  await (sleep(100));
  // run test 2
  await (wrapper.vm.runProgressProgress());
  expect(wrapper.vm.loadingValue).not.toBe(-1);
});

test('mount component - method copyToClipboad', async () => {
  // mock
  serverOK.listen();
  // mount
  clipboardContents = '';
  expect(clipboardContents).to.eq("");
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x' } });
  // run test 1
  await (wrapper.vm.copyToClipboad("abc"));
  expect(clipboardContents).to.eq("abc");
  // end
  clipboardContents = '';
});

test('mount component - method updateLoadingValue', async () => {
  // mock
  serverOK.listen();
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x' } });
  // clear
  await (wrapper.unmount());
  await (wrapper.vm.runProgressProgress = () => { });
  // run test 1
  await (wrapper.vm.loadingValue = 100);
  await (wrapper.vm.updateLoadingValue());
  expect(wrapper.vm.loadingValue).to.eq(2);
  // run test 2
  await (wrapper.vm.updateLoadingValue());
  expect(wrapper.vm.loadingValue).to.eq(4);
});

test('mount component - method triggerRemovePublication', async () => {
  // mock
  serverOK.listen();
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x' } });
  // run test 1
  await (wrapper.vm.triggerRemovePublication());
  expect(wrapper.emitted('remove-publication')).to.deep.eq([["x",]]);
});

test('mount component - method processError', async () => {
  // mock
  serverOK.listen();
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x' } });
  // run test 1
  const t1 = {} as LiteratureServiceResponse;
  t1.status = LiteratureServiceResponseStatusEnum.ApiError;
  await (wrapper.vm.status = Status.Init)
  await (wrapper.vm.processError(t1));
  expect(wrapper.vm.currentClasses).to.equal('hint-error');
  expect(wrapper.vm.errorMessage).to.equal('An error occured with EBI REST API.');
  // run test 2
  const t2 = {} as LiteratureServiceResponse;
  t2.status = LiteratureServiceResponseStatusEnum.NoResult;
  await (wrapper.vm.status = Status.Init)
  await (wrapper.vm.processError(t2));
  expect(wrapper.vm.currentClasses).to.equal('hint-warning');
  expect(wrapper.vm.errorMessage).to.equal('No Results matching this identifier.');
  // run test 3
  const t3 = {} as LiteratureServiceResponse;
  t3.status = LiteratureServiceResponseStatusEnum.UnknownError;
  await (wrapper.vm.status = Status.Init)
  await (wrapper.vm.processError(t3));
  expect(wrapper.vm.currentClasses).to.equal('hint-error');
  expect(wrapper.vm.errorMessage).to.equal('An unknown error occurred.');
});

test('mount component - test click copyClipBoard', async () => {
  // mock
  serverOK.listen();
  // mount
  clipboardContents = '';
  expect(clipboardContents).to.eq("");
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x' } });
  // init test 
  await (wrapper.vm.status = Status.Success)
  await (wrapper.vm.loadPublicationDataFromIdentifier = () => { });
  await (sleep(50))
  await (wrapper.vm.publication = {
    apa_html: "apa",
    doi: "doi",
    pubmed_cid: "3",
    pubmed_id: "pmid",
    reference_html: "ref <em>20YY</em>",
    title: '6',
  });
  const button = wrapper.findAll('.fa.fa-clipboard');
  expect(button.length).to.eq(4);
  // btn 1
  clipboardContents = '';
  expect(clipboardContents).to.eq("");
  await button[0].trigger('click');
  expect(clipboardContents).to.eq("ref 20YY");
  // btn 2
  clipboardContents = '';
  expect(clipboardContents).to.eq("");
  await button[1].trigger('click');
  expect(clipboardContents).to.eq("doi");
  // btn 3
  clipboardContents = '';
  expect(clipboardContents).to.eq("");
  await button[2].trigger('click');
  expect(clipboardContents).to.eq("apa");
  // btn 4
  clipboardContents = '';
  expect(clipboardContents).to.eq("");
  await button[3].trigger('click');
  expect(clipboardContents).to.eq("pmid");
});

test('mount component - test click', async () => {
  // mock
  serverOK.listen();
  // mount
  clipboardContents = '';
  expect(clipboardContents).to.eq("");
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x', isReadonly: false, } });
  await sleep(50);
  // init test 
  await (wrapper.vm.status = Status.Success)
  await (wrapper.vm.publication = {
    apa_html: "apa",
    doi: "doi",
    pubmed_cid: "3",
    pubmed_id: "pmid",
    reference_html: "5",
    title: '6',
  });
  const button = wrapper.find('.fas.fa-trash');
  // btn 
  await button.trigger('click');
  expect(clipboardContents).to.eq("");
  expect(wrapper.emitted('remove-publication')).to.deep.eq([["x",]]);
});

test('mount component - method loadPublicationDataFromIdentifier', async () => {
  // mock
  serverOK.listen();
  //
  const successResponse = {
    status: 0,
    publication: {
      doi: undefined,
      pubmed_id: undefined,
      pubmed_cid: undefined,
      // title: 'Sample Title',
      apa_html: '',
      reference_html: ' undefined',
    },
    error: '',
  } as unknown;
  // mount
  const wrapper = mount(MthPfDisplayPublication, { global, props: { publicationIdentifier: 'x' } });
  await sleep(50);
  // Ensure that the status and publication properties are updated correctly
  expect(wrapper.vm.status).toBe(Status.Success);
  expect(wrapper.vm.publication).toEqual((successResponse as LiteratureServiceResponse).publication);
})

function sleep(milliseconds: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, milliseconds);
  });
}
