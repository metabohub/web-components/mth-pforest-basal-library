import { mount } from '@vue/test-utils';
import { expect, test, } from "vitest";
import vuetify from '@/plugins/vuetify';
import { useTheme } from 'vuetify/lib/framework.mjs';

// core test
import MthPfSwitchTheme from '@/components/MthPfSwitchTheme.vue';
import { LOCALSTORAGE_KEY_THEME } from '@/components/MthPfSwitchTheme.vue';


const global = {
  plugins: [vuetify]
}

// test basic
test('component exists', async () => {
  // mount
  expect(MthPfSwitchTheme).toBeTruthy();
});

test('mount component - no option', async () => {
  // mount
  const wrapper = mount(MthPfSwitchTheme, { global });
  // Check initial theme value
  expect(wrapper.vm.theme).toBe('light');
  // Check if the icon is correct
  expect(wrapper.vm.themeIcon).toBe('fas fa-sun fa-1x');
  expect(wrapper.find("i").exists()).toBe(true);
  expect(wrapper.find("i").classes()).toContain("fas");
  expect(wrapper.find("i").classes()).toContain("fa-moon");
  // Check if the label is not dispayed
  expect(wrapper.text()).toBe("");
});

test('mount component - set option displayThemeText', async () => {
  const wrapper = mount(MthPfSwitchTheme, { global, props: { displayThemeText: true } });
  // test prop
  expect(wrapper.vm.displayThemeText).toBe(true);
  // Check initial theme value
  expect(wrapper.vm.theme).toBe('light');
  // Check if the icon is correct
  expect(wrapper.vm.themeIcon).toBe('fas fa-sun fa-1x');
  expect(wrapper.find("i").exists()).toBe(true);
  // Check if the label is not dispayed
  expect(wrapper.text()).toBe("Theme:");
  // set props
  await wrapper.setProps({ displayThemeText: false });
  // test prop
  expect(wrapper.vm.displayThemeText).toBe(false);
  // Check if the icon is correct
  expect(wrapper.vm.themeIcon).toBe('fas fa-sun fa-1x');
  expect(wrapper.find("i").exists()).toBe(true);
  // Check if the label is not dispayed
  expect(wrapper.text()).toBe("");
});

test('mount component - set option displayThemeIcon', async () => {
  // mount
  const wrapper = mount(MthPfSwitchTheme, { global, props: { displayThemeIcon: false } });
  // test prop
  expect(wrapper.vm.displayThemeIcon).toBe(false);
  // Check if the icon is correct
  expect(wrapper.find("i").exists()).toBe(false);
  // Check if the label is not dispayed
  expect(wrapper.text()).toBe("");
  // set props
  await wrapper.setProps({ displayThemeIcon: true });
  // test prop
  expect(wrapper.vm.displayThemeIcon).toBe(true);
  // Check if the icon is correct
  expect(wrapper.find("i").exists()).toBe(true);
  // Check if the label is not dispayed
  expect(wrapper.text()).toBe("");
});

test('mount component - method toggleTheme | init dark', async () => {
  // init
  const wrapper = mount(MthPfSwitchTheme, {
    global, setup() {
      const themObjVal = useTheme()
      themObjVal.global.name.value = 'dark';
      return {
        // get current theme
        themeObj: themObjVal,
        // model and icon
        theme: null,
        themeIcon: "",
        // display options
        isDisplayThemeText: true,
        isDisplayThemeIcon: true,
      }
    }
  });
  // Check initial theme value
  expect(wrapper.vm.theme).toBe('dark');
  expect(wrapper.vm.themeObj.global.name.value).toBe('dark');
  // trigger change
  await wrapper.vm.toggleTheme();
  // check if values have been updated
  expect(wrapper.vm.themeObj.global.name.value).toBe('light');
});

test('mount component - method toggleTheme | init light', async () => {
  // init
  const wrapper = mount(MthPfSwitchTheme, {
    global, setup() {
      return {
        // get current theme
        themeObj: useTheme(),
        // model and icon
        theme: null,
        themeIcon: "",
        // display options
        isDisplayThemeText: true,
        isDisplayThemeIcon: true,
      }
    }
  });
  // Check initial theme value
  expect(wrapper.vm.theme).toBe('light');
  expect(wrapper.vm.themeObj.global.name.value).toBe('light');
  // trigger change
  await wrapper.vm.toggleTheme();
  // check if values have been updated
  expect(wrapper.vm.themeObj.global.name.value).toBe('dark');
});

test('mount component - init localStorage', async () => {
  // init
  localStorage.setItem(LOCALSTORAGE_KEY_THEME, "dark");
  expect(MthPfSwitchTheme).toBeTruthy()
  const wrapper = mount(MthPfSwitchTheme, { global });
  // Check initial theme value
  expect(wrapper.vm.theme).toBe('dark');
  // Check if the icon is correct
  expect(wrapper.vm.themeIcon).toBe('fas fa-moon fa-1x');
  expect(wrapper.find("i").exists()).toBe(true);
  expect(wrapper.find("i").classes()).toContain("fas");
  expect(wrapper.find("i").classes()).toContain("fa-moon");
  // Check if the label is not dispayed
  expect(wrapper.text()).toBe("");
  // clear
  localStorage.clear();
});

test('mount component - test ', async () => {
  // init
  localStorage.setItem(LOCALSTORAGE_KEY_THEME, "light");
  expect(MthPfSwitchTheme).toBeTruthy()
  // init
  const wrapper = mount(MthPfSwitchTheme, { global });
  // Check initial theme value
  expect(wrapper.vm.theme).toBe('light');
  // change
  wrapper.vm.theme = 'updated';
  // check if changer
  expect(wrapper.vm.theme).toBe('updated');
  // clear
  localStorage.clear();
});

test('mount component - init theme as "light" from parent', async () => {
  // init
  expect(MthPfSwitchTheme).toBeTruthy()
  const wrapper = mount(MthPfSwitchTheme, { global, props: { currentTheme: 'light' } });
  // Check initial theme value
  expect(wrapper.vm.theme).toBe('light');
  // clear
  localStorage.clear();
});

test('mount component - init theme as "dark" from parent', async () => {
  // init
  expect(MthPfSwitchTheme).toBeTruthy()
  const wrapper = mount(MthPfSwitchTheme, { global, props: { currentTheme: 'dark' } });
  // Check initial theme value
  expect(wrapper.vm.theme).toBe('dark');
  // clear
  localStorage.clear();
});

test('mount component - change theme as "dark" from parent', async () => {
  // init
  expect(MthPfSwitchTheme).toBeTruthy()
  const wrapper = mount(MthPfSwitchTheme, { global, props: { currentTheme: 'light' } });
  // Check initial theme value
  expect(wrapper.vm.theme).toBe('light');
  expect(wrapper.vm.themeIcon).toBe('fas fa-sun fa-1x');
  expect(wrapper.find("i").exists()).toBe(true);
  // change currentTheme prop. from parent
  await wrapper.setProps({ currentTheme: 'dark' });
  // Check new theme value
  expect(wrapper.vm.theme).toBe('dark');
  expect(wrapper.vm.themeIcon).toBe('fas fa-moon fa-1x');
  expect(wrapper.find("i").exists()).toBe(true);
  // clear
  localStorage.clear();
});

test('mount component - change theme as "light" from parent', async () => {
  // init
  expect(MthPfSwitchTheme).toBeTruthy()
  const wrapper = mount(MthPfSwitchTheme, { global, props: { currentTheme: 'dark' } });
  // Check initial theme value
  expect(wrapper.vm.theme).toBe('dark');
  expect(wrapper.vm.themeIcon).toBe('fas fa-moon fa-1x');
  expect(wrapper.find("i").exists()).toBe(true);
  // change currentTheme prop. from parent
  await wrapper.setProps({ currentTheme: 'light' });
  // Check new theme value
  expect(wrapper.vm.theme).toBe('light');
  expect(wrapper.vm.themeIcon).toBe('fas fa-sun fa-1x');
  expect(wrapper.find("i").exists()).toBe(true);
  // clear
  localStorage.clear();
});