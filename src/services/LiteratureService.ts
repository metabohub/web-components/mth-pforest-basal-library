// OpenAPI generated code
import { // 
    Result, //
    SearchResult, //
} from '@/openapi-ts/ebi'

export {
    type Publication, //
    type LiteratureServiceResponse, //
    LiteratureServiceResponseStatusEnum, //
    getPublicationFromIdentifier, //
}

/**
 * Define a Publication as an object
 */
interface Publication {
    // identifier
    doi: string | undefined,
    pubmed_id: string | undefined,
    pubmed_cid: string | undefined,
    // core
    title: string | undefined,
    apa_html: string | undefined,
    reference_html: string | undefined,
}

/** 
 * Define a Literature Service Response
 */
interface LiteratureServiceResponse {
    status: LiteratureServiceResponseStatusEnum,
    publication: Publication,
    error: string,
}

/** 
 *  Enum used to define a Literature Service Response Status
 */
const enum LiteratureServiceResponseStatusEnum {
    Success,
    NoResult,
    ApiError,
    UnknownError,
}

/**
 * Get a publication from it DOI
 * @param identifier the publication's DOI or PubMed ID
 */
function getPublicationFromIdentifier(identifier: String): Promise<LiteratureServiceResponse> {
    // init
    const identifierSanitized = encodeURIComponent(`${identifier}`.replace(/https?:\/\/(dx\.)?doi.org\//, ''));
    // run
    return new Promise((resolve, reject) => {
        // https://www.ebi.ac.uk/europepmc/webservices/rest/search?query=10.1038%2Fsdata.2016.18&format=json&domain=literature
        const basePath = "https://www.ebi.ac.uk/europepmc/webservices/rest/";
        const format = 'json';
        const domain = 'literature';
        // fetch!!!
        fetch(`${basePath}search?query=${identifierSanitized}&format=${format}&domain=${domain}`, {
            method: 'GET',
        })
            .then((response) => {
                // response.status === 200 ? "OK" : "KO";
                if (response.status === 200) {
                    return response.json();
                }
                // error
                const rejectResponse = {} as LiteratureServiceResponse;
                rejectResponse.status = LiteratureServiceResponseStatusEnum.UnknownError;
                rejectResponse.error = `${response.status} | ${response.statusText}`;
                return reject(rejectResponse);
            }).then((data: SearchResult) => {
                const publication = processEbiPublicationResponse(data);
                return resolve(publication);
            })
            .catch((err) => {
                // wtf? 
                const rejectResponse = {} as LiteratureServiceResponse;
                rejectResponse.status = LiteratureServiceResponseStatusEnum.ApiError;
                rejectResponse.error = `${err}`;
                return reject(rejectResponse);
            });
    });
}

/**
 * Process a EBI "getPublicationInformation" success response into a LiteratureServiceResponse object
 * @param searchResult  the raw EBI success response as a SearchResult object
 * @returns a response as a LiteratureServiceResponse object
 */
function processEbiPublicationResponse(searchResult: SearchResult): LiteratureServiceResponse {
    // init
    const response = {} as LiteratureServiceResponse;
    response.status = LiteratureServiceResponseStatusEnum.UnknownError;
    const query = searchResult.request?.queryString ?? '';
    const rawResultsList = searchResult.resultList?.result ?? [];
    // test response
    switch (searchResult.hitCount) {
        // case no response
        case 0:
            response.status = LiteratureServiceResponseStatusEnum.NoResult;
            break;
        // case one response
        case 1:
            if (searchResult.hitCount === 1 && //
                rawResultsList.length === 1) {
                const rawResult = rawResultsList[0];
                response.status = LiteratureServiceResponseStatusEnum.Success;
                response.publication = processEbiPublicationInformationSingleResult(rawResult);
            }
            break;
        // case more than one response
        default:
            for (const result in rawResultsList) {
                const resultObj = rawResultsList[result];
                // if match 
                if (query === resultObj.doi ||//
                    query === resultObj.pmcid ||//
                    query === resultObj.pmid) {
                    response.status = LiteratureServiceResponseStatusEnum.Success;
                    response.publication = processEbiPublicationInformationSingleResult(resultObj);
                    break;
                }
            }
            break;
    }
    return response;
}

/**
 * Process a EBI "getPublicationInformation" single result into a Publication object
 * @param result the raw EBI success response as a Result object
 * @returns a response as a Publication object
 */
function processEbiPublicationInformationSingleResult(result: Result): Publication {
    // init
    const publication = {} as Publication;
    // process identifier
    publication.doi = result.doi;
    publication.pubmed_id = result.pmid;
    publication.pubmed_cid = result.pmcid;
    // process core
    publication.apa_html = processEbiPublicationInformationSingleResultAsHtmlApa(result);
    publication.reference_html = processEbiPublicationInformationSingleResultAsHtmlReference(result);
    // return
    return publication;
}

/**
 * Process a EBI "getPublicationInformation" single result publication reference compliant string
 * @param result  the raw EBI success response as a Result object
 * @returns a HTML formatted string matching the Publication Reference
 */
function processEbiPublicationInformationSingleResultAsHtmlReference(result: Result): string {
    // process authors
    const authorRef = getAuthorsRef(result);
    // process year
    const yearPub = result.pubYear;
    // return 
    return `${authorRef} ${yearPub}`;
}

/**
 * Process a EBI "getPublicationInformation" single result publication APA compliant string
 * @param result  the raw EBI success response as a Result object
 * @returns a HTML formatted string matching the Publication APA
 */
function processEbiPublicationInformationSingleResultAsHtmlApa(result: Result): string {
    let apa = ``;
    // authors
    apa += (getAllAuthors(result));
    // pub year / title / journal title
    apa += (getApaMainData(result));
    // volume / issue
    apa += (getVolumeAndIssue(result));
    if (result.pageInfo != null && result.pageInfo !== "") {
        apa += (", " + result.pageInfo);
    }
    // check if ends with a dot
    if (apa !== '' && "." !== (apa.substring(apa.length - 1))) {
        apa += (".");
    }
    // check if get DOI
    if (result.doi != null && result.doi != "") {
        apa += (" " + result.doi + ".");
    }
    // return
    return apa.toString();
}

/**
 * Process a EBI "getPublicationInformation" single result to extract author(s) reference
 * @param result  the raw EBI success response as a Result object
 * @returns a HTML formatted string of author(s) reference
 */
function getAuthorsRef(result: Result) {
    let authorRef = "";
    if (result.authorString && result.authorString.length !== 0) {
        const authors = result.authorString.split(", ");
        // first author
        authorRef += `${cleanAuthorName(authors[0])}`;
        // second author or "et al."
        if (authors.length == 2) {
            authorRef += ` &amp; ${cleanAuthorName(authors[1])}`;
        }
        if (authors.length > 2) {
            authorRef += " <em>et al.</em>";
        }
    }
    return authorRef;
}

/**
 * Clean a raw author name to extract only its lastname
 * @param inputName  the raw author name
 * @returns just the author's lastname
 */
function cleanAuthorName(inputName: string) {
    return (inputName.split(" "))[0];
}

/**
 * Process a EBI "getPublicationInformation" single result to extract all authors
 * @param result  the raw EBI success response as a Result object
 * @returns a HTML formatted string of all authors
 */
function getAllAuthors(result: Result) {
    let authorsAsApa = "";
    if (result.authorString != null && result.authorString !== "") {
        const authors = result.authorString.split(", ");
        const lastAuthorObj = (authors[authors.length - 1]);
        for (const author in authors) {
            const authorObj = authors[author];
            if (authorsAsApa !== "") {
                if (authorObj === lastAuthorObj) {
                    authorsAsApa += (". &amp; ");
                } else {
                    authorsAsApa += ("., ");
                }
            }
            authorsAsApa += (authorObj.trim().replace(" ", ", "));
        }
        authorsAsApa += (" ");
    }
    return authorsAsApa;
}

/**
 * Process a EBI "getPublicationInformation" single result to extract APA main data
 * @param result  the raw EBI success response as a Result object
 * @returns a HTML formatted string of APA main data
 */
function getApaMainData(result: Result) {
    let apa = '';
    if (result.pubYear != null && result.pubYear !== '') {
        apa += ("(" + result.pubYear + "). ");
    }
    if (result.title != null && result.title !== '') {
        apa += ("" + result.title + " ");
    }
    if (result.journalTitle != null && result.journalTitle !== '') {
        apa += ("<em>" + result.journalTitle + "</em>");
    }
    return apa.toString();
}

/**
 * Process a EBI "getPublicationInformation" single result to extract Volume and Issue data
 * @param result  the raw EBI success response as a Result object
 * @returns a HTML formatted string of Volume and Issue data
 */
function getVolumeAndIssue(result: Result) {
    let volumeAndIssue = '';
    if (result.journalVolume != null && result.journalVolume !== '') {
        volumeAndIssue += (result.journalVolume);
    }
    if (result.issue != null && result.issue !== '') {
        volumeAndIssue += ("(" + result.issue + ")");
    }
    if (volumeAndIssue !== '') {
        volumeAndIssue = (`, ${volumeAndIssue}`);
    }
    return volumeAndIssue;
}