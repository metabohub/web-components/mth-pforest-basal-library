// Composables
import { createWebHashHistory, createRouter } from 'vue-router'

// in-house views
import About from "@/views/About.vue";
import HowToInstall from "@/views/HowToInstall.vue";

// tutorials
import TutoMthPfDisplayExternalId from "@/views/TutoMthPfDisplayExternalId.vue";
import TutoMthPfDisplayProperty from "@/views/TutoMthPfDisplayProperty.vue";
import TutoMthPfDisplayLink from '@/views/TutoMthPfDisplayLink.vue';
import TutoMthPfDisplayPublication from '@/views/TutoMthPfDisplayPublication.vue';
import TutoMthPfSwitchTheme from "@/views/TutoMthPfSwitchTheme.vue";
import TutoMthPfDisplayScoredValue from '@/views/TutoMthPfDisplayScoredValue.vue';

// define routes
const routes = [
  // default page
  {
    path: '/',
    redirect: '/how-to-install'
  },
  // main views
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/how-to-install",
    name: "How to Install",
    component: HowToInstall,
  },
  // tutorial views
  {
    path: "/tuto-display-external-id",
    name: "TutoMthPfDisplayExternalId",
    component: TutoMthPfDisplayExternalId,
  },
  {
    path: "/tuto-display-property",
    name: "TutoMthPfDisplayProperty",
    component: TutoMthPfDisplayProperty,
  },
  {
    path: "/tuto-display-link",
    name: "TutoMthPfDisplayLink",
    component: TutoMthPfDisplayLink,
  },
  {
    path: "/tuto-display-publication",
    name: "TutoMthPfDisplayPublication",
    component: TutoMthPfDisplayPublication,
  },
  {
    path: "/tuto-switch-theme",
    name: "TutoMthPfSwitchTheme",
    component: TutoMthPfSwitchTheme,
  },
  {
    path: "/tuto-display-scored-value",
    name: "TutoMthPfDisplayScoredValue",
    component: TutoMthPfDisplayScoredValue,
  },
];

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
})

export default router;
