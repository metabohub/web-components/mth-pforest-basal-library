/* tslint:disable */
/* eslint-disable */
/**
 * EBI API - overwitten methods
 * API specification to support \"EBI - REST API\" operations
 *
 * The version of the OpenAPI document: 2.1.0
 * Contact: www-prod@ebi.ac.uk
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */



/**
 * 
 * @export
 * @interface ResultDbCrossReferenceList
 */
export interface ResultDbCrossReferenceList {
    /**
     * 
     * @type {Array<string>}
     * @memberof ResultDbCrossReferenceList
     */
    'dbName'?: Array<string>;
}

