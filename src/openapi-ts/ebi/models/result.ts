/* tslint:disable */
/* eslint-disable */
/**
 * EBI API - overwitten methods
 * API specification to support \"EBI - REST API\" operations
 *
 * The version of the OpenAPI document: 2.1.0
 * Contact: www-prod@ebi.ac.uk
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import { ResultBookOrReportDetails } from './result-book-or-report-details';
// May contain unused imports in some cases
// @ts-ignore
import { ResultDbCrossReferenceList } from './result-db-cross-reference-list';
// May contain unused imports in some cases
// @ts-ignore
import { ResultFullTextIdList } from './result-full-text-id-list';
// May contain unused imports in some cases
// @ts-ignore
import { ResultTmAccessionTypeList } from './result-tm-accession-type-list';

/**
 * 
 * @export
 * @interface Result
 */
export interface Result {
    /**
     * the result\'s identifier
     * @type {string}
     * @memberof Result
     */
    'id'?: string;
    /**
     * the result\'s source
     * @type {string}
     * @memberof Result
     */
    'source'?: string;
    /**
     * the result\'s pmid
     * @type {string}
     * @memberof Result
     */
    'pmid'?: string;
    /**
     * the result\'s pmcid
     * @type {string}
     * @memberof Result
     */
    'pmcid'?: string;
    /**
     * the result\'s DOI
     * @type {string}
     * @memberof Result
     */
    'doi'?: string;
    /**
     * the result\'s Title
     * @type {string}
     * @memberof Result
     */
    'title'?: string;
    /**
     * the result\'s \'list of authors\'
     * @type {string}
     * @memberof Result
     */
    'authorString'?: string;
    /**
     * the result\'s journal title
     * @type {string}
     * @memberof Result
     */
    'journalTitle'?: string;
    /**
     * the result\'s journal issue
     * @type {string}
     * @memberof Result
     */
    'issue'?: string;
    /**
     * the result\'s journal volume
     * @type {string}
     * @memberof Result
     */
    'journalVolume'?: string;
    /**
     * the result\'s journal publication year
     * @type {string}
     * @memberof Result
     */
    'pubYear'?: string;
    /**
     * the result\'s journal ISSN
     * @type {string}
     * @memberof Result
     */
    'journalIssn'?: string;
    /**
     * the result\'s publication pages in journal
     * @type {string}
     * @memberof Result
     */
    'pageInfo'?: string;
    /**
     * the result\'s publication type
     * @type {string}
     * @memberof Result
     */
    'pubType'?: string;
    /**
     * the result\'s publication \'open access\' status
     * @type {string}
     * @memberof Result
     */
    'isOpenAccess'?: string;
    /**
     * the result\'s publication \'in EPMC\' status
     * @type {string}
     * @memberof Result
     */
    'inEPMC'?: string;
    /**
     * the result\'s publication \'in PMC\' status
     * @type {string}
     * @memberof Result
     */
    'inPMC'?: string;
    /**
     * the result\'s publication \'has PDF\' status
     * @type {string}
     * @memberof Result
     */
    'hasPDF'?: string;
    /**
     * the result\'s publication \'has Book\' status
     * @type {string}
     * @memberof Result
     */
    'hasBook'?: string;
    /**
     * the result\'s publication \'has Suppl\' status
     * @type {string}
     * @memberof Result
     */
    'hasSuppl'?: string;
    /**
     * the result\'s publication \'citation count\'
     * @type {number}
     * @memberof Result
     */
    'citedByCount'?: number;
    /**
     * the result\'s publication \'has References\' status
     * @type {string}
     * @memberof Result
     */
    'hasReferences'?: string;
    /**
     * the result\'s publication \'has Text Mined Terms\' status
     * @type {string}
     * @memberof Result
     */
    'hasTextMinedTerms'?: string;
    /**
     * the result\'s publication \'has Db Cross References\' status
     * @type {string}
     * @memberof Result
     */
    'hasDbCrossReferences'?: string;
    /**
     * 
     * @type {ResultDbCrossReferenceList}
     * @memberof Result
     */
    'dbCrossReferenceList'?: ResultDbCrossReferenceList;
    /**
     * the result\'s publication \'has Labs Links\' status
     * @type {string}
     * @memberof Result
     */
    'hasLabsLinks'?: string;
    /**
     * the result\'s publication \'has TM Accession Numbers\' status
     * @type {string}
     * @memberof Result
     */
    'hasTMAccessionNumbers'?: string;
    /**
     * the result\'s publication \'first index\' date
     * @type {string}
     * @memberof Result
     */
    'firstIndexDate'?: string;
    /**
     * the result\'s publication \'first publication\' date
     * @type {string}
     * @memberof Result
     */
    'firstPublicationDate'?: string;
    /**
     * 
     * @type {ResultFullTextIdList}
     * @memberof Result
     */
    'fullTextIdList'?: ResultFullTextIdList;
    /**
     * 
     * @type {ResultBookOrReportDetails}
     * @memberof Result
     */
    'bookOrReportDetails'?: ResultBookOrReportDetails;
    /**
     * 
     * @type {number}
     * @memberof Result
     */
    'versionNumber'?: number;
    /**
     * 
     * @type {ResultTmAccessionTypeList}
     * @memberof Result
     */
    'tmAccessionTypeList'?: ResultTmAccessionTypeList;
}

