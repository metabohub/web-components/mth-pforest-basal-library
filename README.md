# MetaboHUB / PeakForest - Basal Library

[![pipeline status](https://forgemia.inra.fr/metabohub/web-components/mth-pforest-basal-library/badges/dev/pipeline.svg)](https://forgemia.inra.fr/metabohub/web-components/mth-pforest-basal-library/-/commits/dev)
[![coverage report](https://forgemia.inra.fr/metabohub/web-components/mth-pforest-basal-library/badges/dev/coverage.svg)](https://forgemia.inra.fr/metabohub/web-components/mth-pforest-basal-library/-/commits/dev)
[![Latest Release](https://forgemia.inra.fr/metabohub/web-components/mth-pforest-basal-library/-/badges/release.svg)](https://forgemia.inra.fr/metabohub/web-components/mth-pforest-basal-library/-/releases)

## Metadata

* authors: <nils.paulhe@inrae.fr>, <celian.barbe@inrae.fr>
* creation date: `2023-05-26`
* main usage: Vue.js webcomponents use for [PeakForest V3 Web-Applications](https://TODO-PForest-URL/).

TODO add a fiew words about PeakForest project

## Getting Started

This WebComponent library is compatible with projects using [Vue.js 3](https://vuejs.org/) and [TypeScript](https://www.typescriptlang.org/).
This project is based on [Vue.js](https://vuejs.org/), [Vite](https://vitejs.dev/guide/) and [Vuetify](https://vuetifyjs.com/en/) frameworks.

### Prerequisites

Note: this section is for developers who want to get involved into this project.

* Node 18.16.0 (mandatory) - we recommand you to use [Node Version Manager](https://github.com/nvm-sh/nvm)
* Vue 3 (mandatory) - install with `npm install -g vue@latest`
* git (recommended) - used during build to extract current commit hash/tag into showcase webapp footer.
* curl (very optional) - only used for CI/CD

### Installing

Note: this section is for developers who want to implement and use this library into their own project(s).
To add the library to your [Node.js](https://nodejs.org/) project, please enter the following commands:

<!-- TODO: host it on npm global repository -->

```bash
# if you already have installed the library, you must remove the previous one:
npm remove @metabohub/peakforest-basal-library

# config. the GitLab registry (dev. version)
# WARNING: you must create a GitLab API Token with this rights: api, read_api, read_repository
npm config set -- //forgemia.inra.fr/api/v4/projects/11779/packages/npm/:_authToken=$GITLAB_API_TOKEN
npm config set @metabohub/peakforest-basal-library:registry https://forgemia.inra.fr/api/v4/projects/11779/packages/npm/

# to install the library in your current Node.js project:
npm i --save @metabohub/peakforest-basal-library
```

Then you must import the library in your `src/main.ts` file:

```js
// import the library and its styles
import { MetabohubPeakForestBasalLibrary } from '@metabohub/peakforest-basal-library';
import '@metabohub/peakforest-basal-library/dist/src/components/main.css';

const app = createApp(App);
registerPlugins(app); // <= import your plugin like vuetify, ...
app.use(MetabohubPeakForestBasalLibrary); // <= add this line!
app.mount('#app');
```

Finally you can add our WebComponent in you own Vue components:

```html
<template>
  <div class="demo">
    <mth-pf-display-property property-label="Hello" property-value="World" />
  </div>
</template>

<script lang="ts">

// from Vue core
import { ref } from "vue";

export default {
  name: "Demo",
  setup() {};
};
</script>
<style lang="scss"></style>
```

<!-- TODO 
## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```bash
Give an example
```

### And coding style tests

Explain what these tests test and why

```bash
Give an example
```
-->

## Developments

<!-- ### Edit Forum endpoint connection

If you edit `src/assets/doc_forum-openapi.json` file, you must re-generate the endpoint client code thanks `npm run openapi-generator` code. -->

### Library build and publish

Build and publish the library

```bash
# install node modules listed into 'package.json' / 'package-lock.json' files
npm install
# build the library
npm run build:lib
npm pack
# publish the library
npm publish
```

Notes:

* the publication registery is set into these files:
  * [.npmrc](.npmrc).
  * [package.json](package.json) - key "publishConfig".
* the publication is automatic during CI/CD process; see [.gitlab-ci.yml](.gitlab-ci.yml) file.
* during the GitLab CI/CD pipeline, the previous package with the same version identifier is removed thanks:
  * `curl` commands - to get all published packages in targeted GitLab packages registery and remove a specific one\
  (it require a GitLab API token with correct authorizations under `MASTER_TOKEN` CI/CD variable)

### Standalone showcase build

Build the standalone version with these commands

```bash
# install node modules listed into 'package.json' / 'package-lock.json' files
npm install
# either build the DEV standalone version
npm run build:dev
# or build the PROD standalone version
npm run build:prod
```

Please refer to [.env](.env), [.env.developement](.env.developement) and [.env.production](.env.production) files to see changes between PROD and DEV versions.

Note: you can run the standalone version into "dev mode" thanks `npm run dev` command.
<!--
## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.
-->

## Versioning

We use [GitLab](https://forgemia.inra.fr/metabohub/web-components/mth-pforest-basal-library/) for versioning.
For the versions available, see the [tags on this repository](https://forgemia.inra.fr/metabohub/web-components/mth-pforest-basal-library/tags).

Consult the [Changelog file](CHANGELOG.md) for further informations about versions and changes.

## Authors

* **Nils Paulhe** - *Initial work* - MetaboHUB, INRAE, PFEM.
* **Célian Barbe** - *Initial work* - MetaboHUB, INRAE, PFEM.

See also the list of [contributors](https://forgemia.inra.fr/metabohub/web-components/mth-pforest-basal-library/-/graphs/dev) who participated in this project.

## License

This project is licensed under the `Apache 2` License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [MetaboHUB](https://www.metabohub.fr/home.html)
* [INRAE](https://www.inrae.fr/en)
